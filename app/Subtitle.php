<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subtitle extends Model
{
    protected $table ="subtitles_appearance";
    protected $fillable = ['text_size', 'text_color', 'background_color', 'user_id'];

    public function users()
    {
        return $this->hasOne('App\User', '' );
    }
}
