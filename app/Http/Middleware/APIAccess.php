<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\Hash;

class APIAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($request->input('token') != null) {

            $values = explode('-', $request->input('token'));
            $userID = $values[0];
            $token = $values[1];
            $data = User::where('id', $userID)->first();

            if (Hash::check($token, $data->api_token)) {

                return $next($request);
            }

            return response("Acceso denegado", 403);
  
        }

        else {
            return response('Acceso denegado', 403);
        }


        
    }
}
