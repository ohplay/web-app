<?php

namespace App\Http\Controllers;

use App\Config;
use App\Http\Requests;
use App\Menu;
use App\Package;
use App\PaypalSubscription;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;
use Redirect;
use Session;
use Validator;
use App\Mail\SendInvoiceMailable;
use Illuminate\Support\Facades\Mail;

use Srmklive\PayPal\Services\ExpressCheckout;

class PaypalController extends Controller
{
    protected $provider;

    public function __construct() {
        $this->provider = new ExpressCheckout();
    }
	/**
     * Store a details of payment with paypal.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postPaymentWithpaypal(Request $request)
    {
    	$plan = Package::findOrFail($request->plan_id);
    	$currency_code = strtoupper(Config::first()->currency_code);
        $this->provider->setCurrency($currency_code);
        //Definimos la data del pago
        $data = [
            'items' => [
                [
                'name' => $plan->name,
                'price' => $plan->amount,
                'qty' => 1
                ],
            ]
        ];

        $invoice = Carbon::now()->timestamp;
        /*$end_date = null;

        if ($plan->interval == 'month') {
            $end_date = Carbon::now()->addMonths($plan->interval_count);
        } else if ($plan->interval = 'year') {
            $end_date = Carbon::now()->addYears($plan->interval_count);
        } else if ($plan->interval == 'week') {
            $end_date = Carbon::now()->addWeeks($plan->interval_count);
        } else if ($plan->interval == 'day') {
            $end_date = Carbon::now()->addDays($plan->interval_count);
        }

        $auth = Auth::user();

        $created_subscription = PaypalSubscription::create([
            'user_id' => $auth->id,
            'payment_id' => $payment_id,
            'user_name' => $auth->name,
            'package_id' => $plan->id,
            'price' => $plan->amount,
            'status' => 0,
            'method' => 'paypal',
            'subscription_from' => $current_date,
            'subscription_to' => $end_date
        ]);*/

        //Definimos la factura y urls de retorno y cancelación
        $data['invoice_id'] = $invoice;
        $data['invoice_description'] = "Order #{$invoice}";
        $data['return_url'] = route('getPaymentStatus');
        $data['cancel_url'] = route('getPaymentFailed');

        //Seteamos el monto
        $data['total'] = $plan->amount;

        $response = $this->provider->setExpressCheckout($data, true);

        if (!$response['paypal_link']){
            return back()->with('deleted', 'Some error occur, sorry for inconvenient');
        }

        Session::put('plan', $plan);
        Session::put('invoice', $invoice);

        return redirect($response['paypal_link']);
    }

    public function getPaymentStatus(Request $request)
    {
        $menus = Menu::all();
        $user_email = Auth::user()->email;
        $com_email = Config::findOrFail(1)->w_email;

        Session::put('user_email', $user_email);
        Session::put('com_email', $com_email);

        if (empty($request->get('PayerID')) || empty($request->get('token'))) {
            return back()->with('deleted', 'Payment failed');
        }

        $token = $request->get('token');

        $response = $this->provider->getExpressCheckoutDetails($token);

        if (!in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
            return redirect('/')->with(['code' => 'danger', 'message' => 'Error processing PayPal payment']);
        }

        $plan = Session::get('plan');
        $invoice = Session::get('invoice');
        //Session::forget('plan');
        
        $end_date = null;
        if ($plan->interval == 'month') {
            $end_date = Carbon::now()->addMonths($plan->interval_count);
        } else if ($plan->interval == 'year') {
            $end_date = Carbon::now()->addYears($plan->interval_count);
        } else if ($plan->interval == 'week') {
            $end_date = Carbon::now()->addWeeks($plan->interval_count);
        } else if ($plan->interval == 'day') {
            $end_date = Carbon::now()->addDays($plan->interval_count);
        }

        //dd($end_date->toAtomString());

        $profile_desc = 'Order #'.$invoice;
        $currency_code = strtoupper(Config::first()->currency_code);

        $data = [
            'PROFILESTARTDATE' => Carbon::now()->toAtomString(),
            'DESC' => $profile_desc,
            'BILLINGPERIOD' => ucfirst($plan->interval), // Can be 'Day', 'Week', 'SemiMonth', 'Month', 'Year'
            'BILLINGFREQUENCY' => 1, // 
            'INITAMT' => $plan->amount,
            'AMT' => $plan->amount, // Billing amount for each billing cycle
            'CURRENCYCODE' => $currency_code // Currency code  // (Optional) Change it accordingly
        ];

        if ($plan->trial_period_days) {
            $data['TRIALBILLINGPERIOD'] = 'Day';  // (Optional) Can be 'Day', 'Week', 'SemiMonth', 'Month', 'Year'
            $data['TRIALBILLINGFREQUENCY'] = $plan->trial_period_days; // (Optional) set 12 for monthly, 52 for yearly 
            $data['TRIALTOTALBILLINGCYCLES'] = 1; // (Optional) Change it accordingly
            $data['TRIALAMT'] = 0;
        }

        //dd($data, $token);

        $response = $this->provider->createRecurringPaymentsProfile($data, $token);

        //dd($response);
        
        /** dd($result);exit; /** DEBUG RESULT, remove it later **/
        if ($response["ACK"] == 'Success') { 
            /** it's all right **/
            /** Here Write your database logic like that insert record or value in database if you want **/
            $auth = Auth::user();
            $current_date = Carbon::now();

            $created_subscription = PaypalSubscription::create([
                'user_id' => $auth->id,
                'payment_id' => $response["PROFILEID"],
                'user_name' => $auth->name,
                'package_id' => $plan->id,
                'price' => $plan->amount,
                'status' => 1,
                'method' => 'paypal',
                'subscription_from' => $current_date,
                'subscription_to' => $end_date
            ]);

            //dd($created_subscription);

            if ($created_subscription) {
                Mail::send('user.invoice', ['paypal_sub' => $created_subscription, 'invoice' => null], function($message) {
                    $message->from(Session::get('com_email'))->to(Session::get('user_email'))->subject('Invoice');
                });
                Session::forget('user_email');
                Session::forget('com_email');
            }

            if (isset($menus) && count($menus) > 0) {
              return redirect()->route('home', $menus[0]->slug)->with('added', 'Your are now a subscriber !');
            }
            return redirect('/')->with('added', 'Your are now a subscriber !');
        }

		return redirect('/')->with('deleted', 'Payment failed');
    }

    public function getPaymentFailed()
    {
        return redirect('/')->with('deleted', 'Payment failed');
    }

}
