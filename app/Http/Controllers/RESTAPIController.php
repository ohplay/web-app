<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\TvSeries;
use App\Episode;
use App\Season;
use App\Videolink;
use App\MenuVideo;
use App\Movie;
use App\Director;
use App\User;
use App\Actor;
use App\Genre;

class RESTAPIController extends Controller
{

    protected $TMDB_API_KEY;

    function __construct() {
        $this->TMDB_API_KEY = env('TMDB_API_KEY');
    }

    // Public Methods
    public function gettoken(Request $request) {

        $token = Str::random(60);

        $userID = (String) Auth::id();
        
        $updateToken = $this->storageToken($token);

        if (isset($updateToken)) {

            echo "Your new secret token is: ". $userID.'-'.$token
            . "<br />"
            . "An encrypted version of your token will be stored."
            . "<br />"
            . "Keep it somewhere safe or refresh the page to create a new one.";

        }
        
    }

    public function test_request(Request $request) {

            return json_encode(['test_result' => 'Welcome to the REST API for OHPLAY']);

    }

    public function checkmovie(Request $request, $TMDB_ID) {
        
        if ($this->isStoredMovie($TMDB_ID)) {

            return json_encode(true);

        }

        return json_encode(false);

    }

    public function storemovie(Request $request, $TMDB_ID) {

        $TMDB_TRAILER = NULL;

        $THUMBNAIL = NULL;

        $POSTER = NULL;

        $TMDB_DIRECTORS = collect();

        $TMDB_ACTORS = collect();

        $TMDB_GENRES = collect();

        $request->validate([

            'source_file' => 'required',

            'assets_thumbnails' => 'required',

            'menu' => 'required'

        ]);

        if (!$this->isStoredMovie($TMDB_ID)) {

            $URL_TMDB_GET_MOVIE = "https://api.themoviedb.org/3/movie/" . $TMDB_ID . "?api_key= " . $this->TMDB_API_KEY . "&language=es";
            
            $movie_data = json_decode(@file_get_contents($URL_TMDB_GET_MOVIE), true);

            if ($movie_data != NULL) {

                $statusCode = $this->responseStatus('https://api.themoviedb.org/3/movie/' . $TMDB_ID . '/videos?api_key=' . $this->TMDB_API_KEY);

                if ($statusCode == 200) {

                    $trailer_movie_data = json_decode(@file_get_contents('https://api.themoviedb.org/3/movie/' . $TMDB_ID . '/videos?api_key=' . $this->TMDB_API_KEY), TRUE);

                    if (isset($trailer_movie_data['results']) && count($trailer_movie_data['results']) > 0) {

                        $TMDB_TRAILER = 'https://youtu.be/' . $trailer_movie_data['results'][0]['key'];

                    }

                }

                $THUMBNAIL_PATH = $movie_data['poster_path'];

                $content = @file_get_contents('https://image.tmdb.org/t/p/w500/' . $THUMBNAIL_PATH);

                $name_image = 'tmdb_' . substr($THUMBNAIL_PATH, strrpos($THUMBNAIL_PATH, '/') + 1);

                if ($content != NULL) {

                    $TMDB_IMAGE = Storage::disk('imdb_poster_movie')->put($name_image, $content);

                    if ($TMDB_IMAGE != NULL) {

                        $THUMBNAIL = $name_image;

                    }

                }

                $POSTER_PATH = $movie_data['backdrop_path'];
                
                $content = @file_get_contents('https://image.tmdb.org/t/p/original/' . $POSTER_PATH);

                $name_image = 'tmdb_' . substr($POSTER_PATH, strrpos($POSTER_PATH, '/') + 1);
                
               if ($content != NULL) {

                    $TMDB_IMAGE = Storage::disk('imdb_backdrop_movie')->put($name_image, $content);

                    if ($TMDB_IMAGE != NULL) {

                        $POSTER = $name_image;

                    }

               }

               $credits_data = json_decode(@file_get_contents('https://api.themoviedb.org/3/movie/' . $TMDB_ID . '/credits?api_key=' . $this->TMDB_API_KEY), TRUE);

               foreach($credits_data['crew'] as $person) {

                    if ($person['job'] == 'Director') {

                        $check_director_db = Director::where('tmdb_id', $person['id'])->first();

                        if ($check_director_db == NULL) {

                            $DIRECTOR_IMAGE_PATH = "https://image.tmdb.org/t/p/w500/" . $person['profile_path'];

                            $director_content = @file_get_contents($DIRECTOR_IMAGE_PATH);

                            $director_image_name = 'tmdb_' . substr($person['profile_path'], strrpos($person['profile_path'], '/') + 1);

                            $DIRECTOR = "";

                            if ($director_content != NULL) {

                                $STORE_DIRECTOR = Storage::disk('director_image_path')->put($director_image_name, $director_content);

                                if ($STORE_DIRECTOR != NULL) {

                                    $DIRECTOR = $director_image_name;

                                }

                            }

                            $director_in_db = Director::create([

                                'tmdb_id' => $person['id'],

                                'name' => $person['name'],

                                'image' => $DIRECTOR

                            ]);

                            if ($director_in_db != NULL) {

                                $TMDB_DIRECTORS->push($director_in_db->id);

                            }

                        }

                        else { 

                            $TMDB_DIRECTORS->push($check_director_db->id);

                        }

                    }

               }

               $TMDB_DIRECTORS = $TMDB_DIRECTORS->flatten();

               for ($i = 0; $i < 5; $i++) {

                    $resultCreateActor = $this->actorCreate($credits_data['cast'][$i]);

                    $TMDB_ACTORS->push($resultCreateActor->id);

               }

               $TMDB_ACTORS = $TMDB_ACTORS->flatten();

               foreach ($movie_data['genres'] as $genre) {

                    $resultCreateGenre = $this->genreCreate($genre);

                    $TMDB_GENRES->push($resultCreateGenre->id);

               }

               $TMDB_GENRES = $TMDB_GENRES->flatten();

               $movie_in_db = Movie::create([
                   'title' => $movie_data['original_title'],
                   'tmdb_id' => $TMDB_ID,
                   'duration' => $movie_data['runtime'],
                   'tmdb' => 'Y',
                   'director_id' => substr($TMDB_DIRECTORS, 1, -1),
                   'actor_id' => substr($TMDB_ACTORS, 1, -1),
                   'genre_id' => substr($TMDB_GENRES, 1, -1),
                   'trailer_url' => $TMDB_TRAILER,
                   'subtitle' => 0,
                   'featured' => 0,
                   'series' => 0,
                   'detail' => $movie_data['overview'],
                   'rating' => $movie_data['vote_average'],
                   'publish_year' => substr($movie_data['release_date'], 0, 4),
                   'released' => $movie_data['release_date'],
                   'maturity_rating' => 'All age',
                   'thumbnail' => $THUMBNAIL,
                   'poster' => $POSTER
               ]);

               if ($movie_in_db != NULL) {

                    $vd = Videolink::create([
                        'movie_id' => $movie_in_db->id,
                        'source_file' => $request->input('source_file'),
                        'assets_thumbnails' => $request->input('assets_thumbnails')
                    ]);

                    $menu_ids = explode('-', $request->input('menu'));

                    $this->attachmenu($menu_ids, $movie_in_db->id, 'movie');

                    return response("Success", 201);

               }

            }

        }

    }

    public function storeserie(Request $request, $TMDB_ID) {

        $THUMBNAIL = NULL;

        $POSTER = NULL;

        $TMDB_GENRES = collect();

        $request->validate([

            'menu' => 'required'

        ]);

        if (!$this->isStoredSerie($TMDB_ID)) {

            $URL_TMDB_GET_SERIE = 'https://api.themoviedb.org/3/tv/' . $TMDB_ID . '?api_key=' . $this->TMDB_API_KEY . '&language=es';

            $serie_data = json_decode(@file_get_contents($URL_TMDB_GET_SERIE), TRUE);

            if ($serie_data != NULL) {

                $THUMBNAIL_PATH = $serie_data['poster_path'];

                $thumbnail_file = @file_get_contents('https://image.tmdb.org/t/p/w500/' . $THUMBNAIL_PATH);

                $thumbnail_name = 'tmdb_' . substr($THUMBNAIL_PATH, strrpos($THUMBNAIL_PATH, '/') + 1);

                if ($thumbnail_file != NULL) {

                    $storage_result = Storage::disk('imdb_poster_tv_series')->put($thumbnail_name, $thumbnail_file);

                    if ($storage_result != NULL) {

                        $THUMBNAIL = $thumbnail_name;

                    }

                }

                $POSTER_PATH = $serie_data['backdrop_path'];

                $poster_file = @file_get_contents('https://image.tmdb.org/t/p/original/' . $POSTER_PATH);

                $poster_name = 'tmdb_' . substr($POSTER_PATH, strrpos($POSTER_PATH, '/') + 1);

                if ($poster_file != NULL) {

                    $storage_result = Storage::disk('imdb_backdrop_tv_series')->put($poster_name, $poster_file);

                    if ($storage_result != NULL) {

                        $POSTER = $poster_name;

                    }

                }

                foreach ($serie_data['genres'] as $genre) {

                    $resultCreateGenre = $this->genreCreate($genre);

                    $TMDB_GENRES->push($resultCreateGenre->id);

                }

                $TMDB_GENRES = $TMDB_GENRES->flatten();

                $resultCreateSerie = TvSeries::create([

                    'title' => $serie_data['original_name'],
                    'tmdb_id' => $TMDB_ID,
                    'tmdb' => 'Y',
                    'thumbnail' => $THUMBNAIL,
                    'poster' => $POSTER,
                    'genre_id' => substr($TMDB_GENRES, 1, -1),
                    'detail' => $serie_data['overview'],
                    'rating' => $serie_data['vote_average'],
                    'episode_runtime' => $serie_data['episode_run_time'][0],
                    'maturity_rating' => 'All age'

                ]);

                if ($resultCreateSerie != NULL) {

                    $menu_ids = explode('-', $request->input('menu'));

                    $this->attachmenu($menu_ids, $resultCreateSerie->id, 'serie');

                    return response("Success", 201);

                }

                
            
            }

        }

    }

    public function storeseason(Request $request, $TMDB_ID) {

        $THUMBNAIL = NULL;

        $TMDB_ACTORS = collect();

        $request->validate([

            'season_no' => 'required'

        ]);

        if ($this->isStoredSerie($TMDB_ID)) {

            if (!$this->isStoredSeason($TMDB_ID, $request->input('season_no'))) {

                $URL_TMDB_GET_SEASON = "https://api.themoviedb.org/3/tv/" . $TMDB_ID . "/season/" . $request->input('season_no') . "?api_key=" . $this->TMDB_API_KEY . "&language=es";
    
                $season_data = json_decode(@file_get_contents($URL_TMDB_GET_SEASON), TRUE);
    
                if ($season_data != NULL) {
    
                    $THUMBNAIL_PATH = $season_data['poster_path'];
    
                    $thumbnail_file = @file_get_contents('https://image.tmdb.org/t/p/w500/' . $THUMBNAIL_PATH);
    
                    $thumbnail_name = 'tmdb_' . substr($THUMBNAIL_PATH, strrpos($THUMBNAIL_PATH, '/') + 1);
    
                    if ($thumbnail_file != NULL) {
    
                        $storage_result = Storage::disk('imdb_poster_tv_series')->put($thumbnail_name, $thumbnail_file);
    
                        if ($storage_result) {
    
                            $THUMBNAIL = $thumbnail_name;
    
                        }
    
                    }
    
                    $URL_TMDB_GET_CREDITS = 'https://api.themoviedb.org/3/tv/' . $TMDB_ID . '/season/' . $request->input('season_no') . '/credits?api_key=' . $this->TMDB_API_KEY;
    
                    $credits_data = json_decode(@file_get_contents($URL_TMDB_GET_CREDITS), TRUE);
    
                    if ($credits_data != NULL) {
    
                        for ($i = 0; $i < 5; $i++) {
    
                            $resultCreateActor = $this->actorCreate($credits_data['cast'][$i]);
    
                            $TMDB_ACTORS->push($resultCreateActor->id);
        
                        }
                        
                    }
    
                    $TMDB_ACTORS = $TMDB_ACTORS->flatten();
    
                    $resultSeasonCreate = Season::create([
                        'tv_series_id' => TvSeries::where('tmdb_id', $TMDB_ID)->first()->id,
                        'tmdb_id' => $TMDB_ID,
                        'season_no' => $request->input('season_no'),
                        'tmdb' => 'Y',
                        'publish_year' => substr($season_data['air_date'], 0, 4),
                        'thumbnail' => $THUMBNAIL,
                        'actor_id' => substr($TMDB_ACTORS, 1, -1),
                        'detail' => $season_data['overview']
                    ]);

                    if ($resultSeasonCreate != NULL) {

                        return response('Success', 201);

                    }
    
                }
            }

        }

    }

    public function storeepisode(Request $request, $TMDB_ID) {

        $request->validate([
            'episode_no' => 'required',
            'season_no' => 'required',
            'source_file' => 'required',
            'assets_thumbnails' => 'required'
        ]);

        if ($this->isStoredSeason($TMDB_ID, $request->input('season_no'))) {
            
            if (!$this->isStoredEpisode($TMDB_ID, $request->input('season_no'), $request->input('episode_no'))) {

                $URL_TMDB_GET_EPISODE = "https://api.themoviedb.org/3/tv/" . $TMDB_ID . "/season/" . $request->input('season_no') . "/episode/" . $request->input('episode_no') . "?api_key=" . $this->TMDB_API_KEY . "&language=es";
    
                $episode_data = json_decode(@file_get_contents($URL_TMDB_GET_EPISODE), TRUE);
            
                if ($episode_data != NULL) {

                    $resultCreateEpisode = Episode::create([
                        'seasons_id' => Season::where('tmdb_id', $TMDB_ID)->where('season_no', $request->input('season_no'))->first()->id,
                        'title' => $episode_data['name'],
                        'episode_no' => $request->input('episode_no'),
                        'tmdb' => 'Y',
                        'tmdb_id' => $TMDB_ID,
                        'detail' => $episode_data['overview'],
                        'released' => $episode_data['air_date']
                    ]);



                    if ($resultCreateEpisode != NULL) {

                        Videolink::create([
                            'episode_id' => $resultCreateEpisode->id,
                            'source_file' => $request->input('source_file'),
                            'assets_thumbnails' => $request->input('assets_thumbnails')
                        ]);

                        return response('Sucees', 201);

                    }

                }
            }

        }

        

    }

    // Métodos auxiliares del controlador

    private function actorCreate($ACTOR_TMDB) {

        $ACTOR = "";

        $check_actor_db = Actor::where('tmdb_id', $ACTOR_TMDB['id'])->first();

        if ($check_actor_db == NULL) {

            $ACTOR_IMAGE_PATH = "https://image.tmdb.org/t/p/w500/" . $ACTOR_TMDB['profile_path'];

            $actor_contents = @file_get_contents($ACTOR_IMAGE_PATH);

            $actor_image_name = 'tmdb_' . substr($ACTOR_TMDB['profile_path'], strrpos($ACTOR_TMDB['profile_path'], '/') + 1);

            if ($actor_contents != NULL) {

                $STORE_ACTOR = Storage::disk('actor_image_path')->put($actor_image_name, $actor_contents);

                if ($STORE_ACTOR != NULL) {

                    $ACTOR = $actor_image_name;

                }

            }

            $actor_in_db = Actor::create([
                'tmdb_id' => $ACTOR_TMDB['id'],
                'name' => $ACTOR_TMDB['name'],
                'image' => $ACTOR
            ]);

            if ($actor_in_db != NULL) {

                return $actor_in_db;

            }

        }

        else {

            return $check_actor_db;

        }

    }


    private function genreCreate($IMDB_GENRE) { 

        $check_genre_db = Genre::where('tmdb_id', $IMDB_GENRE['id'])->first();

        if ($check_genre_db == NULL) {

            $genre_in_db = Genre::create([
                'tmdb_id' => $IMDB_GENRE['id'],
                'name' => ['en' => $IMDB_GENRE['name']]
            ]);

            return $genre_in_db;

        }

        else {

            return $check_genre_db;

        }       

    }

    private function attachmenu($menu_ids, $contentid, $type) {

        $name_column; 

        if ($type === 'movie') { $name_column = 'movie_id'; } else { $name_column = 'tv_series_id'; }

        foreach ($menu_ids as $id) {

            MenuVideo::create([

                $name_column => $contentid,

                'menu_id' => $id
                
            ]);

        }

    }

    private function responseStatus($url) {

        $headers = get_headers($url);

        return substr($headers[0], 9, 3);

    }

    private function storageToken($token) {

        return User::where('id', Auth::id())->update(['api_token' => Hash::make($token)]);

    }

    private function getMovieByTMDB_ID($TMDB_ID) {

        return Movie::where('tmdb_id', $TMDB_ID)->first();

    }
    
    private function getSerieByTMDB_ID($TMDB_ID) {

        return TvSeries::where('tmdb_id', $TMDB_ID)->first();

    }

    private function getSeasonsByTMDB_ID($TMDB_ID) {

        return Season::where('tmdb_id', $TMDB_ID);

    }

    private function getSeasonID($TMDB_ID, $season_no) {

        $season = $this->getSeasonsByTMDB_ID($TMDB_ID)->where('season_no', $season_no)->first();

        if ($season != NULL) {

            return $season->id;

        }
        
        return FALSE;

    }

    private function isStoredEpisode($TMDB_ID, $season_no, $episode_no) {
        
        $season_id = $this->getSeasonID($TMDB_ID, $season_no);
        
        if ($season_id != NULL) {

            $episode = Episode::where('seasons_id', $season_id)->where('episode_no', $episode_no)->first();

            if ($episode != NULL) {

                return TRUE;

            }

            else {

                return FALSE;

            }

        }

    }

    private function isStoredMovie($TMDB_ID) { 

        $result = $this->getMovieByTMDB_ID($TMDB_ID);

        if ($result != null) {

            return TRUE;

        }

        else {

            return FALSE;

        } 
    }
    
    private function isStoredSerie($TMDB_ID) { 

        $result = $this->getSerieByTMDB_ID($TMDB_ID);

        if ($result != null) {

            return TRUE;

        }

        else {

            return FALSE;

        } 
    }
    
    private function isStoredSeason($TMDB_ID, $season_no) { 

        $result = $this->getSeasonsByTMDB_ID($TMDB_ID)->where('season_no', $season_no)->first();

        if ($result != null) {

            return TRUE;

        }

        else {

            return FALSE;

        } 
    }

}
