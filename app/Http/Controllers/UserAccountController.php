<?php

namespace App\Http\Controllers;

use App\Mail\SendInvoiceMailable;
use App\Package;
use App\Config;
use App\Menu;
use App\PaypalSubscription;
use App\Subtitle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Stripe\Customer;
use Stripe\Subscription;
use \Stripe\Coupon;
use \Stripe\Stripe;

use Srmklive\PayPal\Services\ExpressCheckout;

class UserAccountController extends Controller
{
    public function __construct() {
        $this->provider = new ExpressCheckout();
    }

    public function index()
    {
        // Set your secret key: remember to change this to your live secret key in production
        Stripe::setApiKey(env('STRIPE_SECRET'));
        $auth = Auth::user();
        if ($auth->stripe_id != null) {
            $customer = Customer::retrieve($auth->stripe_id);
        }
        $plans = Package::all();
        $current_subscription = null;
        if (isset($customer)) {
            $data = $customer->invoices();
            $current_subscription = $data->data[0];
        }
        return view('user.index', compact('plans', 'current_subscription'));
    }


    public function purchase_plan()
    {
        $plans = Package::all();
        return view('user.purchaseplan', compact('plans'));
    }

    public function get_payment($id)
    {
        $plan = Package::findOrFail($id);
        return view('subscribe', compact('plan'));
    }

    public function subscribe(Request $request)
    {
        $menus = Menu::all();
        ini_set('max_execution_time', 80);
        // Set your secret key: remember to change this to your live secret key in production
        Stripe::setApiKey(env('STRIPE_SECRET'));
        $auth = Auth::user();
        $token = $request->stripeToken;
        $coupon_valid = true;
        $coupon = $request->coupon;
        $plan = Package::findOrFail($request->plan);
        if ($coupon != null) {
            try {
                $stripe_coupon = Coupon::retrieve($coupon);
                $coupon_valid = true;
                if ($stripe_coupon->valid == false) {
                    $coupon_valid = false;
                    return back()->with('deleted', 'Coupon has been expired');
                }
            } catch (\Stripe\Error\InvalidRequest $e) {
                $coupon_valid = false;
            }
        }

        if ($coupon_valid) {
            $plan_id = $plan->plan_id;
            $plan_name = $plan->name;
            $purchased = $auth->newSubscription($plan_name, $plan_id)
                ->withCoupon($request->coupon)
                ->create($token);
            if (isset($purchased) || $purchased != null) {
                Mail::to($auth->email)->send(new SendInvoiceMailable());
                if (isset($menus) && count($menus) > 0) {
                    return redirect()->route('home', $menus[0]->slug)->with('added', 'Your are now a subscriber !');
                }
                return redirect('/')->with('added', 'Your are now a subscriber !');
            } else {
                return back()->with('deleted', 'Subscription failed ! Please check your debit or credit card.');
            }
        } else {
            return back()->with('deleted', 'Invalid coupon code');
        }
    }

    public function edit_profile()
    {
        return view('user.edit_profile');
    }

    public function update_profile(Request $request)
    {
        $auth = Auth::user();

        $request->validate([
            'current_password' => 'required'
        ]);

        if (Hash::check($request->current_password, $auth->password)) {
            if ($request->new_email !== null) {
                $request->validate([
                    'new_email' => 'required|email'
                ]);
                $auth->update([
                    'email' => $request->new_email
                ]);
                return back()->with('updated', 'Email has been updated');
            }
            if ($request->new_password !== null) {
                $request->validate([
                    'new_password' => 'required|min:6'
                ]);
                $auth->update([
                    'password' => bcrypt($request->new_password)
                ]);
                return back()->with('updated', 'Password has been updated');
            }

        } else {
            return back()->with("deleted", "Your password doesn't match");
        }
        return back();
    }

    public function history()
    {
        // Set your secret key: remember to change this to your live secret key in production
        Stripe::setApiKey(env('STRIPE_SECRET'));

        $auth = Auth::user();
        $invoices = null;
        $paypal_subscriptions = PaypalSubscription::where('user_id', $auth->id)->get();

        try {
            $customer = Customer::retrieve($auth->stripe_id);
            $invoices = $customer->invoices();
        } catch (\Stripe\Error\InvalidRequest $e) {
            $invoices = null;
        }

        return view('user.history', compact('invoices', 'paypal_subscriptions'));
    }

    public function cancelSub($plan_id)
    {
        $auth = Auth::user();
        $auth->subscription($plan_id)->cancel();
        return back()->with('deleted', 'Subscription has been cancelled');
    }

    public function resumeSub($plan_id)
    {
        $auth = Auth::user();
        $auth->subscription($plan_id)->resume();
        return back()->with('updated', 'Subscription has been resumed');
    }

    public function PaypalCancel()
    {
        $auth = Auth::user();
        $sub = $auth->paypal_subscriptions->last();
        $response = $this->provider->cancelRecurringPaymentsProfile($sub->payment_id);
        if ($response["ACK"] == "Success") {
            $sub->status = 0;
            $sub->save();
            return back()->with('deleted', 'Subscription has been cancelled');
        } else {
            return back()->with('deleted', $response["L_LONGMESSAGE0"]);
        }
    }

    public function PaypalResume()
    {
        $auth = Auth::user();
        $sub = $auth->paypal_subscriptions->last();
        $response = $this->provider->reactivateRecurringPaymentsProfile($sub->payment_id);
        if ($response["ACK"] == "Success") {
            $sub->status = 1;
            $sub->save();
            return back()->with('updated', 'Subscription has been resumed');
        } else {
            return back()->with('deleted', $response["L_LONGMESSAGE0"]);
        }
    }

    public function subtitle_appearance()
    {
        $user = Auth::user();
        $subtitles = Subtitle::where("user_id", $user->id)->first();
        $array = [
            "size_min" => [
                "value" => "18px", "selected" => false
            ],
            "size_medium" => [
                "value" => "28px", "selected" => false
            ],
            "size_max" => [
                "value" => "38px", "selected" => false
            ],
            "text_black" => [
                "value" => "#000000", "selected" => false

            ],
            "text_white" => [
                "value" => "#ffffff", "selected" => false
            ],
            "text_green" => [
                "value" => "#3cc60e", "selected" => false
            ],
            "text_blue" => [
                "value" => "#1b2be2", "selected" => false
            ],
            "text_red" => [
                "value" => "#d61818", "selected" => false
            ],
            "back_black" => [
                "value" => "#000000", "selected" => false
            ],
            "back_white" => [
                "value" => "#ffffff", "selected" => false
            ],
            "back_green" => [
                "value" => "#3cc60e", "selected" => false
            ],
            "back_blue" => [
                "value" => "#1b2be2", "selected" => false
            ],
            "back_red" => [
                "value" => "#d61818", "selected" => false
            ]
        ];
        // Si hay una configuración del usuario
        if($subtitles)
        {
            foreach ($array as $key => $item)
            {
                if($item["value"] === $subtitles->text_size)
                {
                    $array[$key]["selected"] = true;
                }
                if($item["value"] === $subtitles->text_color && str_contains($key, "text"))
                {
                    $array[$key]["selected"] = true;
                }
                if($item["value"] === $subtitles->background_color && str_contains($key, "back"))
                {
                    $array[$key]["selected"] = true;
                }
            }
        }
        return view('user.subtitle_appearance', compact('array', 'subtitles'));
    }

    public function subtitle_save(Request $request)
    {
        $user = Auth::user();
        Subtitle::updateOrCreate(
            ['user_id' => $user->id],
            [
                'text_size' => $request->input('text_size'),
                'text_color' => $request->input('text_color'),
                'background_color' => $request->input('back_color')
            ]
        );
        return redirect()->route('subtitle_appearance');
    }
}
