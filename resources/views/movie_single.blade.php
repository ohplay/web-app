@extends('layouts.theme')

@section('main-wrapper')
<!-- main wrapper -->
<section class="main-wrapper">
	@if(isset($movie))
	@if($movie->poster != null)
	<div id="big-main-poster-block" class="big-main-poster-block" style="background-image: url('{{asset('images/movies/posters/'.$movie->poster)}}');">
		<div class="overlay-bg"></div>
	</div>
	@else
	<div id="big-main-poster-block" class="big-main-poster-block" style="background-image: url('{{asset('images/default-poster.jpg')}}');">
		<div class="overlay-bg"></div>
	</div>
	@endif  
	@endif
	@if(isset($season))
	@if($season->poster != null)
	<div id="big-main-poster-block" class="big-main-poster-block" style="background-image: url('{{asset('images/tvseries/posters/'.$season->poster)}}');">
		<div class="overlay-bg"></div>
	</div>
	@elseif($season->tvseries->poster != null)
	<div id="big-main-poster-block" class="big-main-poster-block" style="background-image: url('{{asset('images/tvseries/posters/'.$season->tvseries->poster)}}');">
		<div class="overlay-bg"></div>
	</div>
	@else
	<div id="big-main-poster-block" class="big-main-poster-block" style="background-image: url('{{asset('images/default-poster.jpg')}}');">
		<div class="overlay-bg"></div>
	</div>
	@endif
	@endif
	<div id="full-movie-dtl-main-block" class="full-movie-dtl-main-block full-movie-dtl-block-custom">
		<div class="container-fluid">
			@if(isset($movie))
			@php
			$subtitles = collect();
			if ($movie->subtitle == 1) {
			$subtitle_list = explode(',', $movie->subtitle_list);
			for($i = 0; $i < count($subtitle_list); $i++) {
			try {
			$subtitle = \App\AudioLanguage::find($subtitle_list[$i])->language;
			$subtitles->push($subtitle);
		} catch (Exception $e) {
	}
}
}
$a_languages = collect();
if ($movie->a_language != null) {
$a_lan_list = explode(',', $movie->a_language);
for($i = 0; $i < count($a_lan_list); $i++) {
try {
$a_language = \App\AudioLanguage::find($a_lan_list[$i])->language;
$a_languages->push($a_language);
} catch (Exception $e) {
}
}
}

$wishlist_check = \Illuminate\Support\Facades\DB::table('wishlists')->where([
['user_id', '=', $auth->id],
['movie_id', '=', $movie->id],
])->first();
// Directors list of movie from model
$directors = collect();
if ($movie->director_id != null) {
$p_directors_list = explode(',', $movie->director_id);
for($i = 0; $i < count($p_directors_list); $i++) {
try {
$p_director = \App\Director::find($p_directors_list[$i])->name;
$directors->push($p_director);
} catch (Exception $e) {

}
}
}

// Actors list of movie from model
$actors = collect();
if ($movie->actor_id != null) {
$p_actors_list = explode(',', $movie->actor_id);
for($i = 0; $i < count($p_actors_list); $i++) {
try {
$p_actor = \App\Actor::find($p_actors_list[$i])->name;
$actors->push($p_actor);
} catch (Exception $e) {

}
}
}

// Genre list of movie from model
$genres = collect();
if (isset($movie->genre_id)){
$genre_list = explode(',', $movie->genre_id);
for ($i = 0; $i < count($genre_list); $i++) {
try {
$genre = \App\Genre::find($genre_list[$i])->name;
$genres->push($genre);
} catch (Exception $e) {

}
}
}

@endphp
<div class="row">
	<div class="col-md-8">
		<div class="full-movie-dtl-block">
			<h2 id="full-movie-name" class="section-heading">{{$movie->title}}</h2>
			<div class="imdb-ratings-block">
				<ul>
					<li>{{$movie->publish_year}}</li>
					<li>{{$movie->duration}} {{$popover_translations->where('key', 'mins')->first->value->value}}</li>
					<li>{{$movie->maturity_rating}}</li>
					<li>IMDB {{$movie->rating}}</li>
					@if($movie->subtitle == 1 && isset($subtitles))
					<li>CC</li>
					<li>
						@for($i = 0; $i < count($subtitles); $i++)
						@if($i == count($subtitles)-1)
						{{$subtitles[$i]}}
						@else
						{{$subtitles[$i]}},
						@endif
						@endfor
					</li>
					@endif
				</ul>
			</div>
			<p>
				{{$movie->detail}}
			</p>
		</div>
		<div class="screen-casting-dtl">
			<ul class="casting-headers">
				<li>{{$home_translations->where('key', 'directors')->first->value->value}}</li>
				<li>{{$home_translations->where('key', 'starring')->first->value->value}}</li>
				<li>{{$home_translations->where('key', 'genres')->first->value->value}}</li>
				<li>{{$popover_translations->where('key', 'subtitles')->first->value->value}}</li>
				<li>{{$home_translations->where('key', 'audio languages')->first->value->value}}</li>
			</ul>
			<ul class="casting-dtl">
				<li>
					@if (count($directors) > 0)
					@for($i = 0; $i < count($directors); $i++)
					@if($i == count($directors)-1)
					<a href="{{url('video/detail/director_search', trim($directors[$i]))}}">{{$directors[$i]}}</a>
					@else
					<a href="{{url('video/detail/director_search', trim($directors[$i]))}}">{{$directors[$i]}}</a>,
					@endif
					@endfor
					@else
					-  
					@endif
				</li>
				<li>
					@if (count($actors) > 0)
					@for($i = 0; $i < count($actors); $i++)
					@if($i == count($actors)-1)
					<a href="{{url('video/detail/actor_search', trim($actors[$i]))}}">{{$actors[$i]}}</a>
					@else
					<a href="{{url('video/detail/actor_search', trim($actors[$i]))}}">{{$actors[$i]}}</a>,
					@endif
					@endfor
					@else
					-  
					@endif
				</li>
				<li>
					@if (count($genres) > 0)
					@for($i = 0; $i < count($genres); $i++)
					@if($i == count($genres)-1)
					<a href="{{url('video/detail/genre_search', trim($genres[$i]))}}">{{$genres[$i]}}</a>
					@else
					<a href="{{url('video/detail/genre_search', trim($genres[$i]))}}">{{$genres[$i]}}</a>,
					@endif
					@endfor
					@else
					-
					@endif
				</li>
				<li>
					@if (count($subtitles) > 0)
					@if($movie->subtitle == 1 && isset($subtitles))
					@for($i = 0; $i < count($subtitles); $i++)
					@if($i == count($subtitles)-1)
					{{$subtitles[$i]}}
					@else
					{{$subtitles[$i]}},
					@endif
					@endfor
					@else
					-
					@endif
					@else
					-
					@endif
				</li>
				<li>
					@if (count($a_languages) > 0)
					@if($movie->a_language != null && isset($a_languages))
					@for($i = 0; $i < count($a_languages); $i++)
					@if($i == count($a_languages)-1)
					{{$a_languages[$i]}}
					@else
					{{$a_languages[$i]}},
					@endif
					@endfor
					@else
					-
					@endif
					@else
					-
					@endif
				</li>
			</ul>
		</div>
		<div id="wishlistelement" class="screen-play-btn-block">
			<a onclick="playVideo({{$movie->id}},'{{$movie->type}}')" class="btn btn-play"><span class="play-btn-icon"><i class="fa fa-play"></i></span> <span class="play-text">{{$popover_translations->where('key', 'play')->first->value->value}}</span></a>
			<div class="btn-group btn-block">
				@if($movie->trailer_url != null || $movie->trailer_url != '')
				<a onclick="playTrailer('{{$movie->trailer_url}}')" class="btn btn-default">{{$popover_translations->where('key', 'watch trailer')->first->value->value}}</a>
				@endif
				@if (isset($wishlist_check->added))
				<a onclick="addWish({{$movie->id}},'{{$movie->type}}')" class="addwishlistbtn{{$movie->id}}{{$movie->type}} btn-default">{{$wishlist_check->added == 1 ? ($popover_translations->where('key', 'remove from watchlist')->first->value->value) : ($popover_translations->where('key', 'add to watchlist')->first->value->value)}}</a>
				@else
				<a onclick="addWish({{$movie->id}},'{{$movie->type}}')" class="addwishlistbtn{{$movie->id}}{{$movie->type}} btn-default">{{$popover_translations->where('key', 'add to watchlist')->first->value->value}}</a>
				@endif
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div id="poster-thumbnail" class="poster-thumbnail-block">
			@if($movie->thumbnail != null || $movie->thumbnail != '')
			<img src="{{asset('images/movies/thumbnails/'.$movie->thumbnail)}}" class="img-responsive" alt="genre-image">
			@else
			<img src="{{asset('images/default-thumbnail.jpg')}}" class="img-responsive" alt="genre-image">  
			@endif
		</div>
	</div>
</div>
@elseif(isset($season))
@php
$subtitles = collect();
if ($season->subtitle == 1) {
$subtitle_list = explode(',', $season->subtitle_list);
for($i = 0; $i < count($subtitle_list); $i++) {
try {
$subtitle = \App\AudioLanguage::find($subtitle_list[$i])->language;
$subtitles->push($subtitle);
} catch (Exception $e) {
}
}
}
$a_languages = collect();
if ($season->a_language != null) {
$a_lan_list = explode(',', $season->a_language);
for($i = 0; $i < count($a_lan_list); $i++) {
try {
$a_language = \App\AudioLanguage::find($a_lan_list[$i])->language;
$a_languages->push($a_language);
} catch (Exception $e) {
}
}
}
$wishlist_check = \Illuminate\Support\Facades\DB::table('wishlists')->where([
['user_id', '=', $auth->id],
['season_id', '=', $season->id],
])->first();
// Actors list of movie from model
$actors = collect();
if ($season->actor_id != null) {
$p_actors_list = explode(',', $season->actor_id);
for($i = 0; $i < count($p_actors_list); $i++) {
try {
$p_actor = \App\Actor::find(trim($p_actors_list[$i]))->name;
$actors->push($p_actor);
} catch (Exception $e) {
}
}
}

// Genre list of movie from model
$genres = collect();
if ($season->tvseries->genre_id != null){
$genre_list = explode(',', $season->tvseries->genre_id);
for ($i = 0; $i < count($genre_list); $i++) {
try {
$genre = \App\Genre::find($genre_list[$i])->name;
$genres->push($genre);
} catch (Exception $e) {
}
}
}
@endphp
<div class="row">
	<div class="col-md-8">
		<div class="full-movie-dtl-block">
			<h2 id="full-movie-name" class="section-heading">{{$season->tvseries->title}}</h2>
			<div class="imdb-ratings-block">
				<ul>
					<li>{{$season->publish_year}}</li>
					<li>{{$season->season_no}} {{$popover_translations->where('key', 'season')->first->value->value}}</li>
					<li>{{$season->tvseries->age_req}}</li>
					<li>IMDB {{$season->tvseries->rating}}</li>
					@if(isset($subtitles))
					<li>CC</li>
					<li>
						@for($i = 0; $i < count($subtitles); $i++)
						@if($i == count($subtitles)-1)
						{{$subtitles[$i]}}
						@else
						{{$subtitles[$i]}},
						@endif
						@endfor
					</li>
					@endif
				</ul>
			</div>
			<p>
				@if ($season->detail != null || $season->detail != '')
				{{$season->detail}}
				@else
				{{$season->tvseries->detail}}  
				@endif
			</p>
		</div>
		<div class="screen-casting-dtl">
			<ul class="casting-headers">
				<li>{{$home_translations->where('key', 'starring')->first->value->value}}</li>
				<li>{{$home_translations->where('key', 'genres')->first->value->value}}</li>
				<li>{{$popover_translations->where('key', 'subtitles')->first->value->value}}</li>
				<li>{{$home_translations->where('key', 'audio languages')->first->value->value}}</li>
			</ul>
			<ul class="casting-dtl">
				<li>
					@if (count($actors) > 0)
					@for($i = 0; $i < count($actors); $i++)
					@if($i == count($actors)-1)
					<a href="{{url('video/detail/actor_search', trim($actors[$i]))}}">{{$actors[$i]}}</a>
					@else
					<a href="{{url('video/detail/actor_search', trim($actors[$i]))}}">{{$actors[$i]}}</a>,
					@endif
					@endfor
					@else
					-  
					@endif
				</li>
				<li>
					@if (count($genres) > 0)
					@for($i = 0; $i < count($genres); $i++)
					@if($i == count($genres)-1)
					<a href="{{url('video/detail/genre_search', trim($genres[$i]))}}">{{$genres[$i]}}</a>
					@else
					<a href="{{url('video/detail/genre_search', trim($genres[$i]))}}">{{$genres[$i]}}</a>,
					@endif
					@endfor
					@else
					-  
					@endif
				</li>
				<li>
					@if (count($subtitles) > 0)
					@for($i = 0; $i < count($subtitles); $i++)
					@if($i == count($subtitles)-1)
					{{$subtitles[$i]}}
					@else
					{{$subtitles[$i]}},
					@endif
					@endfor
					@else
					-  
					@endif
				</li>
				<li>
					@if($season->a_language != null && isset($a_languages))
					@for($i = 0; $i < count($a_languages); $i++)
					@if($i == count($a_languages)-1)
					{{$a_languages[$i]}}
					@else
					{{$a_languages[$i]}},
					@endif
					@endfor
					@else
					-
					@endif
				</li>
			</ul>
		</div>
		<div class="screen-play-btn-block">
			<a onclick="playEpisodes()" class="btn btn-play"><span class="play-btn-icon"><i class="fa fa-play"></i></span> <span class="play-text">{{$popover_translations->where('key', 'play')->first->value->value}}</span></a>
			<div id="wishlistelement" class="btn-group btn-block">
				<div>
					@if (isset($wishlist_check->added))
					<a onclick="addWish({{$season->id}},'{{$season->type}}')" class="addwishlistbtn{{$season->id}}{{$season->type}} btn-default">{{$wishlist_check->added == 1 ? ($popover_translations->where('key', 'remove from watchlist')->first->value->value) : ($popover_translations->where('key', 'add to watchlist')->first->value->value)}}</a>
					@else
					<a onclick="addWish({{$season->id}},'{{$season->type}}')" class="addwishlistbtn{{$season->id}}{{$season->type}} btn-default">{{$popover_translations->where('key', 'add to watchlist')->first->value->value}}</a>
					@endif
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div id="poster-thumbnail" class="poster-thumbnail-block">
			@if($season->thumbnail != null)
			<img src="{{asset('images/tvseries/thumbnails/'.$season->thumbnail)}}" class="img-responsive" alt="genre-image">
			@elseif($season->tvseries->thumbnail != null)
			<img src="{{asset('images/tvseries/thumbnails/'.$season->tvseries->thumbnail)}}" class="img-responsive" alt="genre-image">
			@else
			<img src="{{asset('images/default-thumbnail.jpg')}}" class="img-responsive" alt="genre-image">  
			@endif
		</div>
	</div>
</div>
@endif
</div>
</div>
<!-- movie series -->
@if(isset($movie->movie_series) && $movie->series != 1)
@if(count($movie->movie_series) > 0)
<div class="container-fluid movie-series-section search-section">
	<h5 class="movie-series-heading">Series {{count($movie->movie_series)}}</h5>
	<div>
		@foreach($movie->movie_series as $series)
		@php
		$single_series = \App\Movie::where('id', $series->series_movie_id)->first();
		$wishlist_check = \Illuminate\Support\Facades\DB::table('wishlists')->where([
		['user_id', '=', $auth->id],
		['movie_id', '=', $single_series->id],
		])->first();
		@endphp
		<div class="movie-series-block">
			<div class="row">
				<div class="col-sm-3">
					<div class="movie-series-img">
						@if($single_series->thumbnail != null || $single_series->thumbnail != '')
						<img src="{{asset('images/movies/thumbnails/'.$single_series->thumbnail)}}" class="img-responsive" alt="genre-image">
						@endif
					</div>
				</div>
				<div class="col-sm-7 pad-0">
					<h5 class="movie-series-heading movie-series-name"><a href="{{url('movie/detail', $single_series->id)}}">{{$single_series->title}}</h5>
						<ul class="movie-series-des-list">
							<li>IMDB {{$single_series->rating}}</li>
							<li>{{$single_series->duration}} {{$popover_translations->where('key', 'mins')->first->value->value}}</li>
							<li>{{$single_series->publish_year}}</li>
							<li>{{$single_series->maturity_rating}}</li>
							@if($single_series->subtitle == 1)
							<li>{{$popover_translations->where('key', 'subtitles')->first->value->value}}</li>
							@endif
						</ul>
						<p>
							{{$single_series->detail}}
						</p>
						<div class="des-btn-block des-in-list">
							<a onclick="playVideo({{$single_series->id}}, '{{$single_series->type}}')" class="btn btn-play"><span class="play-btn-icon"><i class="fa fa-play"></i></span> <span class="play-text">{{$popover_translations->where('key', 'play')->first->value->value}}</span></a>
							@if($single_series->trailer_url != null || $single_series->trailer_url != '')
							<a onclick="playTrailer('{{$single_series->trailer_url}}')" class="btn-default">{{$popover_translations->where('key', 'watch trailer')->first->value->value}}</a>
							@endif
							@if (isset($wishlist_check->added))
							<a onclick="addWish({{$single_series->id}},'{{$single_series->type}}')" class="addwishlistbtn{{$single_series->id}}{{$single_series->type}} btn-default">{{$wishlist_check->added == 1 ? ($popover_translations->where('key', 'remove from watchlist')->first->value->value) : ($popover_translations->where('key', 'add to watchlist')->first->value->value)}}</a>
							@else
							<a onclick="addWish({{$single_series->id}},'{{$single_series->type}}')" class="addwishlistbtn{{$single_series->id}}{{$single_series->type}} btn-default">{{$popover_translations->where('key', 'add to watchlist')->first->value->value}}</a>
							@endif
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
	@endif
	@endif
	@if(isset($filter_series) && $movie->series == 1)
	@if(count($filter_series) > 0)
	<div class="container-fluid movie-series-section search-section">
		<h5 class="movie-series-heading">{{$home_translations->where('key', 'series')->first->value->value}} {{count($filter_series)}}</h5>
		<div>
			@foreach($filter_series as $key => $series)
			@php
			$wishlist_check = \Illuminate\Support\Facades\DB::table('wishlists')->where([
			['user_id', '=', $auth->id],
			['movie_id', '=', $series->id],
			])->first();
			@endphp
			<div class="movie-series-block">
				<div class="row">
					<div class="col-sm-3">
						<div class="movie-series-img">
							@if($series->thumbnail != null)
							<img src="{{asset('images/movies/thumbnails/'.$series->thumbnail)}}" class="img-responsive" alt="genre-image">
							@endif
						</div>
					</div>
					<div class="col-sm-7 pad-0">
						<h5 class="movie-series-heading movie-series-name"><a href="{{url('movie/detail', $series->id)}}">{{$series->title}}</a></h5>
						<ul class="movie-series-des-list">
							<li>IMDB {{$series->rating}}</li>
							<li>{{$series->duration}} {{$popover_translations->where('key', 'mins')->first->value->value}}</li>
							<li>{{$series->publish_year}}</li>
							<li>{{$series->maturity_rating}}</li>
							@if($series->subtitle == 1)
							<li>{{$popover_translations->where('key', 'subtitles')->first->value->value}}</li>
							@endif
						</ul>
						<p>
							{{$series->detail}}
						</p>
						<div class="des-btn-block des-in-list">
							<a onclick="playVideo({{$series->id}}, '{{$series->type}}')" class="btn btn-play"><span class="play-btn-icon"><i class="fa fa-play"></i></span> <span class="play-text">{{$popover_translations->where('key', 'play')->first->value->value}}</span></a>
							@if($series->trailer_url != null || $series->trailer_url != '')
							<a onclick="playTrailer('{{$series->trailer_url}}')" class="btn-default">{{$popover_translations->where('key', 'watch trailer')->first->value->value}}</a>
							@endif
							@if (isset($wishlist_check->added))
							<a onclick="addWish({{$series->id}},'{{$series->type}}')" class="addwishlistbtn{{$series->id}}{{$series->type}} btn-default">{{$wishlist_check->added == 1 ? ($popover_translations->where('key', 'remove from watchlist')->first->value->value) : ($popover_translations->where('key', 'add to watchlist')->first->value->value)}}</a>
							@else
							<a onclick="addWish({{$series->id}},'{{$series->type}}')" class="addwishlistbtn{{$series->id}}{{$series->type}} btn-default">{{$popover_translations->where('key', 'add to watchlist')->first->value->value}}</a>
							@endif
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
	@endif
	@endif
	<!-- end movie series -->
	<!-- episodes -->
	@if(isset($season->episodes))
	@if(count($season->episodes) > 0)
	<div class="container-fluid movie-series-section search-section">
		<h5 class="movie-series-heading">{{$home_translations->where('key', 'episodes')->first->value->value}} {{count($season->episodes)}}</h5>
		<div>
			@foreach($season->episodes as $key => $episode)
			<div class="movie-series-block">
				<div class="row">
					<div class="col-sm-3">
						<div class="movie-series-img">
							@if($episode->seasons->thumbnail != null)
							<img src="{{asset('images/tvseries/thumbnails/'.$episode->seasons->thumbnail)}}" class="img-responsive" alt="genre-image">
							@elseif($episode->seasons->tvseries->thumbnail != null)
							<img src="{{asset('images/tvseries/thumbnails/'.$episode->seasons->tvseries->thumbnail)}}" class="img-responsive" alt="genre-image">
							@else
							<img src="{{asset('images/default-thumbnail.jpg')}}" class="img-responsive" alt="genre-image">  
							@endif
						</div>
					</div>
					<div class="col-sm-7 pad-0">
						<a onclick="playEpisode({{$key}})" class="btn btn-play btn-sm-play"><span class="play-btn-icon"><i class="fa fa-play"></i></span> <span class="play-text"><h5 class="movie-series-heading movie-series-name">{{$key+1}}. {{$episode->title}}</h5></span></a>
						<ul class="movie-series-des-list">
							<li>{{$episode->duration}} {{$popover_translations->where('key', 'mins')->first->value->value}}</li>
							<li>{{$episode->released}}</li>
							<li>{{$episode->seasons->tvseries->maturity_rating}}</li>
							<li>
								@if($episode->seasons->subtitle == 1)
								{{$popover_translations->where('key', 'subtitles')->first->value->value}}
								@endif
							</li>
						</ul>
						<p>
							{{$episode->detail}}
						</p>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
	@endif
	@endif
	<!-- end episodes -->
	@if($prime_genre_slider == 1)
	@php
	$all = collect();
	$all_fil_movies = App\Movie::all();
	$all_fil_tv = App\TvSeries::all();
	if (isset($movie)) {
	$genres = explode(',', $movie->genre_id);
} elseif (isset($season)) {
$genres = explode(',', $season->tvseries->genre_id);
}
for($i = 0; $i < count($genres); $i++) {
foreach ($all_fil_movies as $fil_movie) {
$fil_genre_item = explode(',', trim($fil_movie->genre_id));
for ($k=0; $k < count($fil_genre_item); $k++) { 
if (trim($fil_genre_item[$k]) == trim($genres[$i])) {
if (isset($movie)) {
if ($fil_movie->id != $movie->id) {
$all->push($fil_movie);                  
}
} else {
$all->push($fil_movie);
}
}
}
}
}
if (isset($movie)) {
$all = $all->except($movie->id);
}

for($i = 0; $i < count($genres); $i++) {
foreach ($all_fil_tv as $fil_tv) {
$fil_genre_item = explode(',', trim($fil_tv->genre_id));
for ($k=0; $k < count($fil_genre_item); $k++) { 
if (trim($fil_genre_item[$k]) == trim($genres[$i])) {
$fil_tv = $fil_tv->seasons;
if (isset($season)) {
$all->push($fil_tv->except($season->id));
} else {
$all->push($fil_tv);                  
}
}
}
}
}
$all = $all->unique();
$all = $all->flatten();
@endphp
@if (isset($all) && count($all) > 0)
<div class="genre-prime-block">
	<div class="container-fluid">
		<h5 class="section-heading">{{$home_translations->where('key', 'customers also watched')->first->value->value}}</h5>
		<div class="genre-prime-slider owl-carousel">
			@if(isset($all))
			@foreach($all as $key => $item)
			@php
			if ($item->type == 'S') {
			$wishlist_check = \Illuminate\Support\Facades\DB::table('wishlists')->where([
			['user_id', '=', $auth->id],
			['season_id', '=', $item->id],
			])->first();
		} elseif ($item->type == 'M') {
		$wishlist_check = \Illuminate\Support\Facades\DB::table('wishlists')->where([
		['user_id', '=', $auth->id],
		['movie_id', '=', $item->id],
		])->first();
	}
	@endphp
	@if($item->type == 'S')
	<div class="genre-prime-slide">
		<div class="genre-slide-image protip" data-pt-placement="outside" data-pt-title="#prime-mix-description-block{{$item->id}}{{$item->type}}">
			<a href="{{url('show/detail',$item->id)}}">
				@if($item->thumbnail != null)
				<img src="{{asset('images/tvseries/thumbnails/'.$item->thumbnail)}}" class="img-responsive" alt="genre-image">
				@elseif($item->tvseries->thumbnail != null)
				<img src="{{asset('images/tvseries/thumbnails/'.$item->tvseries->thumbnail)}}" class="img-responsive" alt="genre-image">
				@else
				<img src="{{asset('images/default-thumbnail.jpg')}}" class="img-responsive" alt="genre-image">  
				@endif
			</a>
		</div>
		<div id="prime-mix-description-block{{$item->id}}{{$item->type}}" class="prime-description-block">
			<h5 class="description-heading">{{$item->tvseries->title}}</h5>
			<div class="movie-rating">IMDB {{$item->tvseries->rating}}</div>
			<ul class="description-list">
				<li>Season {{$item->season_no}}</li>
				<li>{{$item->publish_year}}</li>
				<li>{{$item->tvseries->age_req}}</li>
				@if($item->subtitle == 1)
				<li>CC</li>
				<li>
					{{$popover_translations->where('key', 'subtitles')->first->value->value}}
				</li>
				@endif
			</ul>
			<div class="main-des">
				@if ($item->detail != null || $item->detail != '')
				<p>{{$item->detail}}</p>
				@else  
				<p>{{$item->tvseries->detail}}</p>
				@endif
				<a href="#"></a>
			</div>
			<div class="des-btn-block">
				<a href="#" onclick="playVideo({{$item->id}},'{{$item->type}}')" class="btn btn-play"><span class="play-btn-icon"><i class="fa fa-play"></i></span> <span class="play-text">{{$popover_translations->where('key', 'play')->first->value->value}}</span></a>
				@if (isset($wishlist_check->added))
				<a onclick="addWish({{$item->id}},'{{$item->type}}')" class="addwishlistbtn{{$item->id}}{{$item->type}} btn-default">{{$wishlist_check->added == 1 ? ($popover_translations->where('key', 'remove from watchlist')->first->value->value) : ($popover_translations->where('key', 'add to watchlist')->first->value->value)}}</a>
				@else
				<a onclick="addWish({{$item->id}},'{{$item->type}}')" class="addwishlistbtn{{$item->id}}{{$item->type}} btn-default">{{$popover_translations->where('key', 'add to watchlist')->first->value->value}}</a>
				@endif
			</div>
		</div>
	</div>
	@elseif($item->type == 'M')
	<div class="genre-prime-slide">
		<div class="genre-slide-image protip" data-pt-placement="outside" data-pt-title="#prime-mix-description-block{{$item->id}}">
			<a href="{{url('movie/detail',$item->id)}}">
				@if($item->thumbnail != null)
				<img src="{{asset('images/movies/thumbnails/'.$item->thumbnail)}}" class="img-responsive" alt="genre-image">
				@endif
			</a>
		</div>
		<div id="prime-mix-description-block{{$item->id}}" class="prime-description-block">
			<h5 class="description-heading">{{$item->title}}</h5>
			<div class="movie-rating">IMDB {{$item->rating}}</div>
			<ul class="description-list">
				<li>{{$item->duration}} {{$popover_translations->where('key', 'mins')->first->value->value}}</li>
				<li>{{$item->publish_year}}</li>
				<li>{{$item->maturity_rating}}</li>
				@if($item->subtitle == 1)
				<li>CC</li>
				<li>
					{{$popover_translations->where('key', 'subtitles')->first->value->value}}
				</li>
				@endif
			</ul>
			<div class="main-des">
				<p>{{$item->detail}}</p>
				<a href="#"></a>
			</div>
			<div class="des-btn-block">
				<a href="#" onclick="playVideo({{$item->id}},'{{$item->type}}')" class="btn btn-play"><span class="play-btn-icon"><i class="fa fa-play"></i></span> <span class="play-text">{{$popover_translations->where('key', 'play')->first->value->value}}</span></a>
				@if($item->trailer_url != null || $item->trailer_url != '')
				<a href="#" onclick="playTrailer('{{$item->trailer_url}}')" class="btn-default">{{$popover_translations->where('key', 'watch trailer')->first->value->value}}</a>
				@endif
				@if (isset($wishlist_check->added))
				<a onclick="addWish({{$item->id}},'{{$item->type}}')" class="addwishlistbtn{{$item->id}}{{$item->type}} btn-default">{{$wishlist_check->added == 1 ? ($popover_translations->where('key', 'remove from watchlist')->first->value->value) : ($popover_translations->where('key', 'add to watchlist')->first->value->value)}}</a>
				@else
				<a onclick="addWish({{$item->id}},'{{$item->type}}')" class="addwishlistbtn{{$item->id}}{{$item->type}} btn-default">{{$popover_translations->where('key', 'add to watchlist')->first->value->value}}</a>
				@endif
			</div>
		</div>
	</div>
	@endif
	@endforeach
	@endif
</div>
</div>
</div>
@endif
@else
@php
$all = collect();
$all_fil_movies = App\Movie::all();
$all_fil_tv = App\TvSeries::all();
if (isset($movie)) {
$genres = explode(',', $movie->genre_id);
} elseif (isset($season)) {
$genres = explode(',', $season->tvseries->genre_id);
}
for($i = 0; $i < count($genres); $i++) {
foreach ($all_fil_movies as $fil_movie) {
$fil_genre_item = explode(',', trim($fil_movie->genre_id));
for ($k=0; $k < count($fil_genre_item); $k++) { 
if (trim($fil_genre_item[$k]) == trim($genres[$i])) {
if (isset($movie)) {
if ($fil_movie->id != $movie->id) {
$all->push($fil_movie);                  
}
} else {
$all->push($fil_movie);
}
}
}
}
}
if (isset($movie)) {
$all = $all->except($movie->id);
}

for($i = 0; $i < count($genres); $i++) {
foreach ($all_fil_tv as $fil_tv) {
$fil_genre_item = explode(',', trim($fil_tv->genre_id));
for ($k=0; $k < count($fil_genre_item); $k++) { 
if (trim($fil_genre_item[$k]) == trim($genres[$i])) {
$fil_tv = $fil_tv->seasons;
if (isset($season)) {
$all->push($fil_tv->except($season->id));
} else {
$all->push($fil_tv);                  
}
}
}
}
}
$all = $all->unique();
$all = $all->flatten();
@endphp
@if (isset($all) && count($all) > 0)
<div class="genre-main-block">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3">
				<div class="genre-dtl-block">
					<h3 class="section-heading">{{$home_translations->where('key', 'customers also watched')->first->value->value}}</h3>
					<p class="section-dtl">{{$home_translations->where('key', 'at the big screen at home')->first->value->value}}</p>
				</div>
			</div>
			<div class="col-md-9">
				<div class="genre-main-slider owl-carousel">
					@if(isset($all))
					@foreach($all as $key => $item)
					@if($item->type == 'S')
					<div class="genre-slide">
						<div class="genre-slide-image">
							<a href="{{url('show/detail/'.$item->id)}}">
								@if($item->thumbnail != null)
								<img src="{{asset('images/tvseries/thumbnails/'.$item->thumbnail)}}" class="img-responsive" alt="genre-image">
								@elseif($item->tvseries->thumbnail != null)
								<img src="{{asset('images/tvseries/thumbnails/'.$item->tvseries->thumbnail)}}" class="img-responsive" alt="genre-image">
								@else
								<img src="{{asset('images/default-thumbnail.jpg')}}" class="img-responsive" alt="genre-image">  
								@endif
							</a>
						</div>
						<div class="genre-slide-dtl">
							<h5 class="genre-dtl-heading"><a href="{{url('show/detail/'.$item->id)}}">{{$item->tvseries->title}}</a></h5>
							<div class="genre-small-info">{{$item->detail != null ? $item->detail : $item->tvseries->detail}}</div>
						</div>
					</div>
					@elseif($item->type == 'M')
					<div class="genre-slide">
						<div class="genre-slide-image">
							<a href="{{url('movie/detail/'.$item->id)}}">
								@if($item->thumbnail != null)
								<img src="{{asset('images/movies/thumbnails/'.$item->thumbnail)}}" class="img-responsive" alt="genre-image">
								@endif
							</a>
						</div>
						<div class="genre-slide-dtl">
							<h5 class="genre-dtl-heading"><a href="{{url('movie/detail/'.$item->id)}}">{{$item->title}}</a></h5>
						</div>
					</div>
					@endif    
					@endforeach
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endif
@endif
</section>
<!-- end main wrapper -->
<!-- Player -->
<div class="video-player">
	<div class="close-btn-block text-right">
		<a class="close-btn" onclick="closeVideo()"></a>
	</div>
	<div id="my_video"></div>
</div>
<!-- End Player -->
@endsection

@section('custom-script')

<script type="text/javascript" src="https://content.jwplatform.com/libraries/i511f6Xb.js"></script>
<script type="text/javascript" src="/js/js.cookie.min.js"></script>

<script>
    // Wishlist Js ( using Vuejs 2 )
    var app = new Vue({
    	el: '#wishlistelement',
    	data: {
    		result: {
    			id: '',
    			type: '',
    		},
    	},
    	methods: {
    		addToWishList(id, type) {
    			this.result.id = id;
    			this.result.type = type;
    			this.$http.post('{{route('addtowishlist')}}', this.result).then((response) => {
    			}).catch((e) => {
    				console.log(e);
    			});
    			this.result.item_id = '';
    			this.result.item_type = '';
    		}
    	}
    });

    var jplayer;

    $(document).ready(function(){
    	$('.main-des').curtail({
    		limit: 120,
    		toggle: true,
    		text: ['{{$popover_translations->where('key', 'less')->first->value->value}}', '{{$popover_translations->where('key', 'read more')->first->value->value}}']
    	});      
  	});

    function playVideo(id, type) {

    	app.$http.get(`{{ url('get-video-data') }}/${id}/${type}`).then((response) => {
    		if ( response.status == 200 ) {

    			if ( response.data.links.length ) {

	    			setTimeout(function () {
	    				jplayer = jwplayer('my_video');
						
						jplayer.setup({
							image: response.data.image,
							sources: response.data.links,
							title: response.data.title,
							controls: true,
						});
						
						jplayer.on('time', function(e) {
							var expiresDate = new Date();
							expiresDate.setTime(new Date().getTime() + (30 * 60 * 24 * 60 * 1000));

							Cookies.set('jwplayer#' + id, Math.floor(e.position) + ':' + jplayer.getDuration(), {expires: expiresDate});
						});

						jplayer.on('ready', function() {
						    var cookieData = Cookies.get('jwplayer#' + id);
						    if ( cookieData ) {
						        var resumeAt = cookieData.split(':')[0],
						            videoDur = cookieData.split(':')[1];

						        if ( parseInt(resumeAt) < parseInt(videoDur) ) {
						            jplayer.seek(resumeAt);
						        }
						    }
						});

						jplayer.on('complete', function() {
							jplayer.remove();
							jplayer = null;
						});
	    			}.bind(this), 300);
	    		}
    		}
    	}).catch((e) => {
    		console.log(e);
    	});

    	$('.video-player').css({
    		"visibility" : "visible",
    		"z-index" : "99999",
    	});

    	$('body').css({
    		"overflow": "hidden"
    	});
    }

    function playTrailer(url) {
    	$('.video-player').css({
    		"visibility" : "visible",
    		"z-index" : "99999",
    	});
    	$('body').css({
    		"overflow": "hidden"
    	});

    	$('#my_video').show();
    }

    function closeVideo() {
    	$('#my_video').hide();
    	
		if ( jplayer != undefined ) {
			jplayer.remove();
			jplayer = null;
		}

    	$('.video-player').css({
    		"visibility" : "hidden",
    		"z-index" : "-99999"
    	});

    	$('body').css({
    		"overflow": "auto"
    	});
    }

    function addWish(id, type) {
    	app.addToWishList(id, type);
    	setTimeout(function() {
    		$('.addwishlistbtn'+id+type).text(function(i, text){
    			return text == "{{$popover_translations->where('key', 'add to watchlist')->first->value->value}}" ? "{{$popover_translations->where('key', 'remove from watchlist')->first->value->value}}" : "{{$popover_translations->where('key', 'add to watchlist')->first->value->value}}";
    		});
    	}, 100);
    }
</script>
@endsection