<?php
use App\Genre;
use App\AudioLanguage;

$all_items = [
    'M' => [],
    'S' => [],
];
?>
@extends('layouts.theme')
@section('main-wrapper')
    <section class="main-wrapper">
        <div>
            @if ( count($home_slides) )
                <div id="home-main-block" class="home-main-block" style="padding-bottom:50px">
                    <div id="home-slider-one" class="home-slider-one owl-carousel owl-theme">
                        @foreach($home_slides as $slide)
                            @if($slide->active == 1)
                                <div class="slider-block">
                                    <div class="slider-image">
                                        @if($slide->movie_id != null)
                                            <a href="{{url('movie/detail', $slide->movie->id)}}">
                                                @if ($slide->slide_image != null)
                                                    <img src="{{asset('images/home_slider/movies/'. $slide->slide_image)}}"
                                                         class="img-responsive" alt="slider-image">
                                                @elseif ($slide->movie->poster != null)
                                                    <img src="{{asset('images/movies/posters/'. $slide->movie->poster)}}"
                                                         class="img-responsive" alt="slider-image">
                                                @endif
                                            </a>
                                        @elseif($slide->tv_series_id != null && isset($slide->tvseries->seasons[0]))
                                            <a href="{{url('show/detail', $slide->tvseries->seasons[0]->id)}}">
                                                @if ($slide->slide_image != null)
                                                    <img src="{{asset('images/home_slider/shows/'. $slide->slide_image)}}"
                                                         class="img-responsive" alt="slider-image">
                                                @elseif ($slide->tvseries->poster != null)
                                                    <img src="{{asset('images/tvseries/posters/'. $slide->tvseries->poster)}}"
                                                         class="img-responsive" alt="slider-image">
                                                @endif
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            @endif
                @if ( count($auth->continueWatchings()) )
                    <div class="genre-prime-block">
                        <div class="container-fluid">
                            <h5 class="section-heading">Continue Watching</h5>
                            <div class="genre-prime-slider owl-carousel owl-theme continue">
                                @include ('partials.continue_watchings', ['item_id' => 0, 'item_type' => ''])
                            </div>
                        </div>
                    </div>
                @endif

            {{-- @if ( count($featured_movies) > 0 )
            <div class="genre-prime-block">
            <div class="container-fluid">
            <h5 class="section-heading">{{$home_translations->where('key', 'featured')->first->value->value}} {{$home_translations->where('key', 'movies')->first->value->value}}</h5>
            <div class="genre-prime-slider owl-carousel owl-theme">
            @foreach($featured_movies as $item)
            @include ('partials.movie_item', ['item' => $item, 'section' => 'featured_movies'])
            @endforeach
            </div>
            </div>
            </div>
            @endif
            @if( count($featured_seasons) > 0 )
            <div class="genre-prime-block">
            <div class="container-fluid">
            <h5 class="section-heading">{{$home_translations->where('key', 'featured')->first->value->value}} {{$home_translations->where('key', 'tv shows')->first->value->value}}</h5>
            <div class="genre-prime-slider owl-carousel owl-theme">
            @foreach($featured_seasons as $item)
            @include ('partials.season_item', ['item' => $item, 'section' => 'featured_seasons'])
            @endforeach
            </div>
            </div>
            </div>
            @endif --}}
            @if ( count($movies) > 0 )
                <div class="genre-prime-block">
                    <div class="container-fluid">
                        <h5 class="section-heading">Movies Recently Added</h5>
                        <div class="genre-prime-slider owl-carousel owl-theme">
                            @php
                                $count=0;
                            @endphp
                            @foreach(array_reverse($movies) as $item)
                                @php
                                    if($count == 20) break;
                                @endphp
                                @include ('partials.movie_item', ['item' => $item, 'section' => 'next_movies'])
                                @php
                                    $count++;
                                @endphp
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
            @if ( count($tvserieses) > 0 )
                <div class="genre-prime-block">
                    <div class="container-fluid">
                        <h5 class="section-heading">TV Seasons Recently Added</h5>
                        <div class="genre-prime-slider owl-carousel owl-theme">
                            @php
                                $count=0;
                            @endphp
                            @foreach(array_reverse($tvserieses) as $series)
                                @php
                                    if($count == 20) break;
                                @endphp

                                @include ('partials.season_item', ['item' => $series, 'section' => 'next_tvseries'])
                                @php
                                    $count++;
                                @endphp
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <div id="wishlistelement"></div>
    </section>
    <div class="video-player">
        <div class="close-btn-block text-right">
            <a class="close-btn" onclick="closeVideo()"></a>
        </div>
        <div id="my_video"></div>
    </div>
@endsection
@section('custom-script')
    @include ('partials.script_play')
@endsection