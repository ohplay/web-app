<?php
use App\Genre;
use App\AudioLanguage;

$all_items = [
    'M' => [],
    'S' => [],
];
?>
@extends('layouts.theme')
@section("customCss")
    <style>
        .ias-spinner {
            color: #fff;
            font-size: 2em;
        }
    </style>
@endsection
@section('main-wrapper')
    <section class="main-wrapper">
        <div>
            @if ( count($home_slides) )
                <div id="home-main-block" class="home-main-block" style="padding-bottom:50px">
                    <div id="home-slider-one" class="home-slider-one owl-carousel owl-theme">
                        @foreach($home_slides as $slide)
                            @if($slide->active == 1)
                                <div class="slider-block">
                                    <div class="slider-image">
                                        @if($slide->movie_id != null)
                                            <a href="{{url('movie/detail', $slide->movie->id)}}">
                                                @if ($slide->slide_image != null)
                                                    <img src="{{asset('images/home_slider/movies/'. $slide->slide_image)}}"
                                                         class="img-responsive" alt="slider-image">
                                                @elseif ($slide->movie->poster != null)
                                                    <img src="{{asset('images/movies/posters/'. $slide->movie->poster)}}"
                                                         class="img-responsive" alt="slider-image">
                                                @endif
                                            </a>
                                        @elseif($slide->tv_series_id != null && isset($slide->tvseries->seasons[0]))
                                            <a href="{{url('show/detail', $slide->tvseries->seasons[0]->id)}}">
                                                @if ($slide->slide_image != null)
                                                    <img src="{{asset('images/home_slider/shows/'. $slide->slide_image)}}"
                                                         class="img-responsive" alt="slider-image">
                                                @elseif ($slide->tvseries->poster != null)
                                                    <img src="{{asset('images/tvseries/posters/'. $slide->tvseries->poster)}}"
                                                         class="img-responsive" alt="slider-image">
                                                @endif
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            @endif
            {{--@if ( count($auth->continueWatchings()) )
                <div class="genre-prime-block">
                    <div class="container-fluid">
                        <h5 class="section-heading">Continue Watching</h5>
                        <div class="genre-prime-slider no-slider col-lg-2 col-md-3 col-xs-6 continue">
                            @include ('partials.continue_watchings', ['item_id' => 0, 'item_type' => ''])
                        </div>
                    </div>
                </div>
            @endif--}}
            @if($menu->slug == "movies")
            <div class="genre-prime-block">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <h5 class="section-heading">All movies available &nbsp;
                                <form action="{{route("home-others", $menu_slug)}}" method="post"
                                        name="yearFilterForm" style="display: inline-block;">

                                    <select name="year" onchange="yearFilterForm.submit()" class="selectpicker"
                                            style="padding: 10px 35px 10px 35px; font-weight: 500; font-family: 'Lato', sans-serif !important; border-radius: 2px; font-size: 18px; text-align: center; background: #3a3a3a; color: #fff; border: none;"
                                            data-style="btn-default">
                                        <option value="year">Year</option>
                                        @foreach($years_list as $year)
                                            <option value="{{$year}}" {{$year_selected == $year ? "selected": ""}}>{{$year}}</option>
                                        @endforeach
                                    </select>
                                    <select name="genre" onchange="yearFilterForm.submit()" class="selectpicker"
                                            style="padding: 10px 35px 10px 35px; font-weight: 500; font-family: 'Lato', sans-serif !important; border-radius: 2px; font-size: 18px; text-align: center; background: #3a3a3a; color: #fff; border: none;"
                                            data-style="btn-default">
                                        <option value="genre">Genre</option>
                                        @foreach($genres_list as $genre)
                                            <option value="{{$genre->id}}" {{$genre_selected == $genre->id ? "selected": ""}}>{{$genre->name}}</option>
                                        @endforeach
                                    </select>
                                    {{csrf_field()}}
                                </form>
                            </h5>
                        </div>
                    </div>
                    @if ( count($peliculas) > 0 )
                    {{--<h5 class="section-heading">Movies Recently Added</h5>--}}
                    @foreach(array_reverse($peliculas) as $item)
                        {{--Ponemos la clase no-slider para evitar que se haga un carousel--}}
                        <div class="genre-prime-slider no-slider col-lg-2 col-md-3 col-xs-6">
                            @if($loop->index == 20)
                                @break;
                            @endif
                            @include ('partials.movie_item', ['item' => $item, 'section' => 'next_movies'])
                        </div>
                    @endforeach
                    @else
                    <div class="genre-prime-slider no-slider col-lg-12 col-md-12 col-xs-12" style="height: 0px; padding: 0px; margin: 0px;"></div>
                    <!--<div class="no-slider col-lg-12 col-md-12 col-xs-12">
                        <h4 style="color: white; margin: 500px 0px 500px 0px">NO RESULT FOUND</h4>
                    </div>-->
                    @endif
                    <div id="pagination">
                        <a class="next"
                            href="{{route("home-others",["menu" => $menu_slug, "pagina" => ++$pagina, "year_selected" => $year_selected, "genre_selected" => $genre_selected])}}"></a>
                    </div>
                </div>
            </div>
            @endif
            @if($menu->slug == "tv-series")
                <div class="genre-prime-block">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6">
                                <h5 class="section-heading">All TV Seasons available &nbsp;
                                    <form style="display: inline-block" action="{{route("home-others", $menu_slug)}}"
                                          method="post" name="yearFilterForm">
                                        <select name="year" onchange="yearFilterForm.submit()" class="selectpicker"
                                                style="padding: 10px 35px 10px 35px; font-weight: 500; font-family: 'Lato', sans-serif !important; border-radius: 2px; font-size: 18px; text-align: center; background: #3a3a3a; color: #fff; border: none;"
                                                data-style="btn-default">
                                            <option value="">Year</option>
                                            @foreach($years_list as $year)
                                                <option value="{{$year}}" {{$year_selected == $year ? "selected": ""}}>{{$year}}</option>
                                            @endforeach
                                        </select>
                                        <select name="genre" onchange="yearFilterForm.submit()" class="selectpicker"
                                            style="padding: 10px 35px 10px 35px; font-weight: 500; font-family: 'Lato', sans-serif !important; border-radius: 2px; font-size: 18px; text-align: center; background: #3a3a3a; color: #fff; border: none;"
                                            data-style="btn-default">
                                            <option value="">Genre</option>
                                            @foreach($genres_list as $genre)
                                                <option value="{{$genre->id}}" {{$genre_selected == $genre->id ? "selected": ""}}>{{$genre->name}}</option>
                                            @endforeach
                                        </select>
                                        {{csrf_field()}}
                                    </form>
                                </h5>
                            </div>
                        </div>
                        @if ( count($series) > 0 )
                        @foreach(array_reverse($series) as $serie)
                            <div class="genre-prime-slider no-slider col-lg-2 col-md-3 col-xs-6">
                                @if($loop->index == 20)
                                    @break;
                                @endif
                                @include ('partials.season_item', ['item' => $serie, 'section' => 'next_tvseries'])
                            </div>
                        @endforeach
                        @else
                        <div class="genre-prime-slider no-slider col-lg-12 col-md-12 col-xs-12" style="height: 0px; padding: 0px; margin: 0px;"></div>
                        <!--<div class="no-slider col-lg-12 col-md-12 col-xs-12">
                            <h4 style="color: white; margin: 500px 0px 500px 0px">NO RESULT FOUND</h4>
                        </div>-->
                        @endif
                        <div id="pagination">
                            <a class="next"
                               href="{{route("home-others",["menu" => $menu_slug, "pagina" => ++$pagina, "year_selected" => $year_selected, "genre_selected" => $genre_selected])}}"></a>
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <div id="wishlistelement"></div>
    </section>
    <div class="video-player">
        <div class="close-btn-block text-right">
            <a class="close-btn" onclick="closeVideo()"></a>
        </div>
        <div id="my_video"></div>
    </div>
@endsection
@section('custom-script')
    @include ('partials.script_play')
    <script src="/js/jquery-ias.min.js"></script>
    <script>
        jQuery(document).ready(function(){

            var ias = jQuery.ias({
                container: '.genre-prime-block',
                item: '.genre-prime-slider',
                pagination: '#pagination',
                next: '.next'
            });
            ias.extension(new IASSpinnerExtension({
                html: "<div class='row'>" +
                "<div class='col-sm-6 col-sm-offset-3 text-center'>" +
                "<div class='ias-spinner'>Loading...</div>" +
                "</div>" +
                "</div>"
            }));
        })
    </script>
@endsection