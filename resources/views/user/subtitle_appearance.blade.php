@extends('layouts.theme')
@section('main-wrapper')
    <!-- main wrapper -->
    <section id="main-wrapper" class="main-wrapper home-page user-account-section">
        <div class="container-fluid">
            <h4 class="heading">Subtitle appearance</h4>
            <ul class="bradcump">
                <li><a href="{{url('account')}}">Dashboard</a></li>
                <li>/</li>
                <li>Subtitle apearance</li>
            </ul>
            <div class="panel-setting-main-block">
                <div class="edit-profile-main-block">
                    <form action="{{route("subtitle_save")}}" method="post">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-5 ">
                                <div id="caja">
                                    <img class="imgcaja" src="/images/house-of-cards.jpg">
                                    <p id="subcontenedor"><span id="sub">Do not go gentle into that good night. Range, range
                                        against <br> the dying of the light
                                    </span></p>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <h5 class="subtitle">Text Size</h5>
                                <input type="hidden" class="type" name="text_size" id="text_size"
                                       value="{{$subtitles ? $subtitles->text_size: ""}}">
                                <button type="button"
                                        class="cuadrado cuadrado-gray size {{$array["size_min"]["selected"] ? "cuadrado-activo":""}}"
                                        data-value="{{$array['size_min']["value"]}}"><span
                                            style="font-size: small">A</span></button>
                                <button type="button"
                                        class="cuadrado cuadrado-gray size {{$array["size_medium"]["selected"] ? "cuadrado-activo":""}}"
                                        data-value="{{$array['size_medium']["value"]}}"><span
                                            style="font-size: large">A</span></button>
                                <button type="button"
                                        class="cuadrado cuadrado-gray size {{$array["size_max"]["selected"] ? "cuadrado-activo":""}}"
                                        data-value="{{$array['size_max']["value"]}}"><span
                                            style="font-size: x-large">A</span>
                                </button>
                            </div>

                            <div class="col-md-4 title">
                                <h5 class="subtitle">Text color</h5>
                                <input type="hidden" class="type" name="text_color" id="text_color"
                                       value="{{$subtitles ? $subtitles->text_color : ""}}">
                                <button type="button"
                                        class="cuadrado cuadrado-black text-color {{$array["text_black"]["selected"] ? "cuadrado-activo":""}}"
                                        data-value="{{$array['text_black']["value"]}}"></button>
                                <button type="button"
                                        class="cuadrado cuadrado-white text-color {{$array["text_white"]["selected"] ? "cuadrado-activo":""}}"
                                        data-value="{{$array['text_white']["value"]}}"></button>
                                <button type="button"
                                        class="cuadrado cuadrado-green text-color {{$array["text_green"]["selected"] ? "cuadrado-activo":""}}"
                                        data-value="{{$array['text_green']["value"]}}"></button>
                                <button type="button"
                                        class="cuadrado cuadrado-blue text-color {{$array["text_blue"]["selected"] ? "cuadrado-activo":""}}"
                                        data-value="{{$array['text_blue']["value"]}}"></button>
                                <button type="button"
                                        class="cuadrado cuadrado-red text-color {{$array["text_red"]["selected"] ? "cuadrado-activo":""}}"
                                        data-value="{{$array['text_red']["value"]}}"></button>
                            </div>

                            <div class="col-md-4 title">
                                <h5 class="subtitle">Background color</h5>
                                <input type="hidden" class="type" name="back_color" id="back_color"
                                       value="{{$subtitles ? $subtitles->background_color : ""}}">
                                <button type="button"
                                        class="cuadrado cuadrado-black back-color {{$array["back_black"]["selected"] ? "cuadrado-activo":""}}"
                                        data-value="{{$array['back_black']["value"]}}"></button>
                                <button type="button"
                                        class="cuadrado cuadrado-white back-color {{$array["back_white"]["selected"] ? "cuadrado-activo":""}}"
                                        data-value="{{$array['back_white']["value"]}}"></button>
                                <button type="button"
                                        class="cuadrado cuadrado-green back-color {{$array["back_green"]["selected"] ? "cuadrado-activo":""}}"
                                        data-value="{{$array['back_green']["value"]}}"></button>
                                <button type="button"
                                        class="cuadrado cuadrado-blue back-color {{$array["back_blue"]["selected"] ? "cuadrado-activo":""}}"
                                        data-value="{{$array['back_blue']["value"]}}"></button>
                                <button type="button"
                                        class="cuadrado cuadrado-red back-color {{$array["back_red"]["selected"] ? "cuadrado-activo":""}}"
                                        data-value="{{$array['back_red']["value"]}}"></button>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 40px;">
                            <div class="col-xs-12">
                                <input type="submit" class="btn btn-success" id="confirm" value="Save">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section("custom-script")
    <script>
        $(document).ready(function () {
            var text_size = $("#text_size").val();
            var text_color = $("#text_color").val();
            var back_color = $("#back_color").val();
            if (text_size != "")
                $("#sub").css("font-size", text_size.replace("18px", "14px").replace("28px","16px").replace("38px","18px"));
            if (text_color != "")
                $("#sub").css("color", text_color);
            if (back_color != "")
                $("#sub").css("background-color", back_color);

        });
        $(".cuadrado").on('click', function () {
            var btn = $(this);
            var value = btn.attr("data-value");
            if (btn.hasClass("cuadrado-activo"))
                value = "";
            if (btn.hasClass("size")) {
                $("#text_size").val(value);
                $(".size").removeClass("cuadrado-activo");
                $("#sub").css("font-size", value.replace("18px", "14px").replace("28px","16px").replace("38px","18px"));
            }
            if (btn.hasClass("text-color")) {
                $("#text_color").val(value);
                $(".text-color").removeClass("cuadrado-activo");
                $("#sub").css("color", value);
            }
            if (btn.hasClass("back-color")) {
                $("#back_color").val(value);
                $(".back-color").removeClass("cuadrado-activo");
                $("#sub").css("background-color", value);
            }
            if (value != "")
                btn.addClass("cuadrado-activo");

        });
    </script>
@endsection