<?php
use App\Genre;
use App\AudioLanguage;

$all_items = [
	'M' => [],
	'S' => [],
];
?>

@extends('layouts.theme')

@section('main-wrapper')
  <!-- main wrapper -->
  <section class="main-wrapper main-wrapper-single-movie-prime">
    @if(isset($filter_video))
      @if(count($filter_video) > 0)
        <div class="container-fluid movie-series-section search-section">
          <h5 class="movie-series-heading">{{count($filter_video)}} {{$home_translations->where('key', 'found for')->first->value->value}} "{{$searchKey}}"</h5>
          <div>
            @foreach($filter_video as $item)

              <div class="movie-series-block">
                <div class="row">
                  <div class="col-sm-3">
                    <div class="movie-series-img">
                      @if($item->type == 'M' && $item->thumbnail != null)
                        <img src="{{asset('images/movies/thumbnails/'.$item->thumbnail)}}" class="img-responsive" alt="genre-image">
                      @elseif($item->type == 'M' && $item->thumbnail == null)  
                        <img src="{{asset('images/default-thumbnail.jpg')}}" class="img-responsive" alt="genre-image">
                      @elseif($item->type == 'S')
                        @if($item->thumbnail != null)
                          <img src="{{asset('images/tvseries/thumbnails/'.$item->thumbnail)}}" class="img-responsive" alt="genre-image">
                        @elseif($item->tvseries->thumbnail != null)
                          <img src="{{asset('images/tvseries/thumbnails/'.$item->tvseries->thumbnail)}}" class="img-responsive" alt="genre-image">
                        @else
                          <img src="{{asset('images/default-thumbnail.jpg')}}" class="img-responsive" alt="genre-image">  
                        @endif
                      @endif
                    </div>
                  </div>
                  <div class="col-sm-7 pad-0">
                    <h5 class="movie-series-heading movie-series-name">
                      @if($item->type == 'M')
                        <a href="{{url('movie/detail', $item->id)}}">{{$item->title}}</a>
                      @elseif($item->type == 'S')
                        <a href="{{url('show/detail', $item->id)}}">{{$item->tvseries->title}}</a>
                      @endif
                    </h5>
                    <ul class="movie-series-des-list">
                      @if($item->type == 'M')
                        <li>IMDB {{$item->rating}}</li>                        
                      @endif
                      @if($item->type == 'S')
                        <li>IMDB {{$item->tvseries->rating}}</li>                        
                      @endif
                      <li>
                        @if($item->type == 'M')
                          {{$item->duration}} {{$popover_translations->where('key', 'mins')->first->value->value}}
                        @else
                         {{$popover_translations->where('key', 'season')->first->value->value}} {{$item->season_no}}
                        @endif
                      </li>
                      @if($item->type == 'M')
                        <li>{{$item->released}}</li>
                      @else
                        <li>{{$item->publish_year}}</li>  
                      @endif
                      <li>
                        @if($item->type == 'M')
                          {{$item->maturity_rating}}
                        @else
                          {{$item->tvseries->maturity_rating}} 
                        @endif
                      </li>
                      @if($item->subtitle == 1)
                        <li>
                          {{$popover_translations->where('key', 'subtitles')->first->value->value}}
                        </li>
                      @endif
                    </ul>
                    <p>
                      @if($item->type == 'M')
                        {{str_limit($item->detail, 360)}}
                      @else
                        @if ($item->detail != null || $item->detail != '')
                          {{$item->detail}}
                        @else  
                          {{str_limit($item->tvseries->detail, 360)}}                        
                        @endif
                      @endif
                    </p>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
        </div>
      @else
        <div class="container-fluid movie-series-section search-section">
          <h5 class="movie-series-heading">0 {{$home_translations->where('key', 'found for')->first->value->value}} "{{$searchKey}}"</h5>
        </div>
      @endif
    @endif
  </section>
  <!-- end main wrapper -->
  <div class="video-player">
    <div class="close-btn-block text-right">
      <a class="close-btn" onclick="closeVideo()"></a>
    </div>
    <video id="my_video" class="video-js movies-js vjs-big-play-centered"
           controls
           preload="auto"
    >
    </video>
    <div class="preview-player-block my-episodes">
      <video id="my_episodes" class="video-js episodes-js vjs-big-play-centered"
             controls
             preload="auto"
      >
      </video>
      <div class="playlist-container  preview-player-dimensions vjs-fluid">
        <ol class="vjs-playlist"></ol>
      </div>
    </div>
  </div>
@endsection
@section('custom-script')
  <script>
    var app = new Vue({
      el: '.des-btn-block',
      data: {
        result: {
          id: '',
          type: '',
        },
      },
      methods: {
        addToWishList(id, type) {
          this.result.id = id;
          this.result.type = type;
          this.$http.post('addtowishlist', this.result).then((response) => {
          }).catch((e) => {
            console.log(e);
          });
          this.result.item_id = '';
          this.result.item_type = '';
        }
      }
    });

    var player;
    var myEpisodes;
    $(document).ready(function(){
      $.protip();
      $('.main-des').curtail({
        limit: 120,
        toggle: true,
        text: ['{{$popover_translations->where('key', 'less')->first->value->value}}', '{{$popover_translations->where('key', 'read more')->first->value->value}}']
      });
      player = videojs('my_video', {
        playbackRates: [.5, 1, 1.5, 2],
        techOrder: ["html5", "youtube", "vimeo"],
        youtube: {ytControls: 2},
        plugins: {
          hotkeys: {
            volumeStep: 0.1,
            seekStep: 5,
            enableModifiersForNumbers: false,
            enableNumbers: false
          },
          videoJsResolutionSwitcher: {
            ui: true,
            default: '480',
            dynamicLabel: true
          }
        }
      });

      myEpisodes = videojs('my_episodes', {
        playbackRates: [.5, 1, 1.5, 2],
        techOrder: ["html5", "youtube", "vimeo"],
        youtube: {ytControls: 2},
        plugins: {
          hotkeys: {
            volumeStep: 0.1,
            seekStep: 5,
            enableModifiersForNumbers: false,
            enableNumbers: false
          },
          videoJsResolutionSwitcher: {
            ui: true,
            default: '360',
            dynamicLabel: true
          }
        }
      });
    });

    function playVideo(id, type) {
      var links;
      var List;
      app.$http.get(`{{ url('get-video-data') }}/${id}/${type}`).then((response) => {
        if ( response.status == 200 ) {
          links = response.data.links;
          List = response.data.episode_data;
          console.log(links);
          setTimeout(function(){
            console.log(response.data.poster);
            console.log(response);
            if (type == 'M') {
              $('#my_video').show();
              $('.my-episodes').hide();
              $('.vjs-control-bar').removeClass('hide-visible');
              if (response.data.poster != null) {
                player.poster('{{ url('images/movies/posters/') }}/'+response.data.poster);
              }
              if (links[0].type == 'video/youtube' || links[0].type == 'video/vimeo') {
                $('.vjs-control-bar').addClass('hide-visible');
                player.src(links[0]); 
              } else {
                  player.updateSrc(links); 
              }
              player.play();
            } else if (type == 'S') {
              $('#my_video').hide();
              $('.my-episodes').show();
              $('.vjs-control-bar').removeClass('hide-visible');
              if (response.data.poster != null) {
                myEpisodes.poster('{{ url('images/tvseries/posters/') }}/'+response.data.poster);
              }
              if (myEpisodes.currentType() == 'video/youtube' || myEpisodes.currentType() == 'video/vimeo') {
                $('.vjs-control-bar').addClass('hide-visible');
              }
              myEpisodes.playlist(List);
              myEpisodes.playlistUi(); 
              myEpisodes.updateSrc(List[myEpisodes.playlist.currentItem()].sources); 
              myEpisodes.play();
              $('.vjs-playlist-item').on('click', function(){
                $('.vjs-control-bar').removeClass('hide-visible');
                setTimeout(function(){
                  console.log(List[myEpisodes.playlist.currentItem()]);
                  console.log(myEpisodes.playlist.currentItem());
                  console.log(myEpisodes.src());
                  myEpisodes.updateSrc(List[myEpisodes.playlist.currentItem()].sources); 
                  if (myEpisodes.currentType() == 'video/youtube' || myEpisodes.currentType() == 'video/vimeo') {
                    $('.vjs-control-bar').addClass('hide-visible');
                  }
                  myEpisodes.play();
                }.bind(this), 300);
              });
            }
          }.bind(this), 300);
        }
      }).catch((e) => {
        console.log(e);
      });

      $('.video-player').css({
        "visibility" : "visible",
        "z-index" : "99999",
      });
      $('body').css({
        "overflow": "hidden"
      });
    }

    function playTrailer(url) {
      $('.video-player').css({
        "visibility" : "visible",
        "z-index" : "99999",
      });
      $('body').css({
        "overflow": "hidden"
      });
      $('#my_video').show();
      $('.vjs-control-bar').removeClass('hide-visible');
      let str = url;
      let youtube_slice_1 = str.slice(0, 14);
      let youtube_slice_2 = str.slice(0, 20);
      if (youtube_slice_1 == "https://youtu." || youtube_slice_2 == "https://www.youtube.")
      {
        $('.vjs-control-bar').addClass('hide-visible');
        player.src({ type: "video/youtube", src: url});
      } else {
        player.src({ type: "video/mp4", src: url});
      }

      setTimeout(function(){
        player.play();
      }, 300);
    }

    function closeVideo() {
      $('#my_video').hide();
      $('.my-episodes').hide();
      $('.video-player').css({
        "visibility" : "hidden",
        "z-index" : "-99999"
      });
      $('body').css({
        "overflow": "auto"
      });
      $('.vjs-control-bar').removeClass('hide-visible');
      player.src(null);
      player.updateSrc(null);
      myEpisodes.src(null);
      myEpisodes.updateSrc(null);
      myEpisodes.playlist(null);
      links = null;
      list = null;
      player.pause();
      myEpisodes.pause();
    }

    function addWish(id, type) {
      app.addToWishList(id, type);
      setTimeout(function() {
        $('.addwishlistbtn'+id+type).text(function(i, text){
          return text == "{{$popover_translations->where('key', 'add to watchlist')->first->value->value}}" ? "{{$popover_translations->where('key', 'remove from watchlist')->first->value->value}}" : "{{$popover_translations->where('key', 'add to watchlist')->first->value->value}}";
        });
      }, 100);
    }
  </script>
@endsection