
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
window.Vue = require('vue');
var VueResource = require('vue-resource');
Vue.use(VueResource);
window.videojs = require('video.js');
require('videojs-hotkeys');
require('videojs-share');
require('videojs-resolution-switcher');
require('videojs-playlist');

window.Vue.http.headers.common['X-CSRF-TOKEN'] = window.Laravel.csrfToken;


/**
 * X-CSRF-TOKEN
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example', require('./components/Example.vue'));
//
// const app = new Vue({
//     el: '#app'
// });3

var delayTimer;
$(document).ready(function() {

    $('.search_form').submit(function(e) {
        e.preventDefault()
    })

    $('input[name="search"]').keyup(function() {
        clearTimeout(delayTimer);
        var query = $('input[name="search"]').val()

        if (!query.length) {
            $('.list-search-response').empty()
            return
        }
        
        $('.list-search-response').empty()
        $('.list-search-response').append('<div>Searching..</div>')

        delayTimer = setTimeout(function() {

            var action = $('.search_form').attr('action');

            $.ajax(action + '?search=' + query)
            .done(function(res) {
                $('.list-search-response').empty()

                if(!res.length) {
                    $('.list-search-response').empty()
                    $('.list-search-response').append('<div>No results found.</div>')
                }

                res.forEach(el => {
                    
                    if (el.type == 'series') {
                        var urlpart = 'show'
                        var title = el.series.title + ' (Season ' + el.season_no + ')'
                    } else if (el.type == 'movies') {
                        var urlpart = 'movie'
                        var title = el.title
                    }

                    var html = '<div><a href="/'+ urlpart +'/detail/'+ el.id +'">'+ title +'</a></div>'
                    $('.list-search-response').append(html)
                });
            })
            .fail(function() {
            })
            .always(function() {
            });

        }, 1000);
    })
})