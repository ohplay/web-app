<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $currentDate = date('Y-M-D h:i:s');

        DB::table('users')->insert([
            'name' => 'Nealth',
            'email' => 'abc@cosa.com',
            'password' => bcrypt('12345gmt'),
            'is_admin' => 1
        ]);

        DB::table('users')->insert([
            'name' => 'Infrative',
            'email' => 'somon@de.com',
            'password' => bcrypt('12345gmt'),
            'is_admin' => 0,
            'created_at' => $currentDate,
            'updated_at' => $currentDate
        ]);

    }
}
