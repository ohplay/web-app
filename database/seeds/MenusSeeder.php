<?php

use Illuminate\Database\Seeder;

class MenusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menus')->insert([
            'name' => '{"en":"Inicio"}',
            'slug' => 'home',
            'position' => 1
        ]);
        DB::table('menus')->insert([
            'name' => '{"en":"Películas"}',
            'slug' => 'movies',
            'position' => 2
        ]);
        DB::table('menus')->insert([
            'name' => '{"en":"Series"}',
            'slug' => 'tv-series',
            'position' => 3
        ]);
    }
}
