<?php

use Illuminate\Database\Seeder;

class HeaderTranslationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('header_translations')->insert([
            'key' => 'dashboard',
            'value' => '{"en":"Dashboard","nl":"Dashboard","de":"Instrumententafel"}'
        ]);
        DB::table('header_translations')->insert([
            'key' => 'faqs',
            'value' => '{"en":"Faq\'s","nl":"Faq\'s","de":"Faq\'s"}'
        ]);
        DB::table('header_translations')->insert([
            'key' => 'sign in',
            'value' => '{"en":"Sign In","nl":"Sign In","de":"Anmelden"}'
        ]);
        DB::table('header_translations')->insert([
            'key' => 'sign out',
            'value' => '{"en":"Sign Out","nl":"Sign Out","de":"Ausloggen"}'
        ]);
        DB::table('header_translations')->insert([
            'key' => 'watchlist',
            'value' => '{"en":"Watchlist","nl":"Watchlist","de":"Beobachtungsliste"}'
        ]);
        DB::table('header_translations')->insert([
            'key' => 'register',
            'value' => '{"en":"Register","nl":"Register","de":"Registrieren"}'
        ]);
    }
}
