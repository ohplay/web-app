<?php

use Illuminate\Database\Seeder;

class PackagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('packages')->insert([
            'plan_id' => 1,
            'name' => '{"en":"Testing"}',
            'currency' => 'USD',
            'amount' => 1.00,
            'interval' => 'month',
            'interval_count' => 1,
            'trial_period_days' => NULL,
            'screen' => NULL,
            'status' => 1,
        ]);
    }
}
