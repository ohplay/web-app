<?php

use Illuminate\Database\Seeder;

class ConfigsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configs')->insert([
            'logo' => 'logo_1541154080logo.png',
            'favicon' => 'favicon.ico',
            'title' => '{"en":"OH Play"}',
            'w_name' => '{"en":"OH Play"}',
            'w_email' => 'contact@nexthour.com',
            'stripe_pub_key' => '',
            'stripe_secret_key' => '',
            'paypal_mar_email' => '',
            'currency_code' => 'USD',
            'currency_symbol' => 'fa fa-dollar',
            'invoice_add' => '{"en":null}',
            'prime_main_slider' => 0,
            'prime_genre_slider' => 1,
            'prime_footer' => 1,
            'prime_movie_single' => 1,
            'terms_condition' => '{"en":"<p>Here goes the terms and condition policy text!<\/p>","nl":"<p>newvious&nbsp;goodesioanos<\/p>"}',
            'privacy_pol' => '{"en":"<p>Here goes the privacy policy text!<br \/>\r\n&nbsp;<\/p>"}',
            'refund_pol' => '{"en":"<p>Here goes the refund policy text!<\/p>"}',
            'copyright' => '{"en":null}',
            'stripe_payment' => 0,
            'paypal_payment' => 1,
            'payu_payment' => 0,
            'preloader' => 0
        ]);
    }
}
