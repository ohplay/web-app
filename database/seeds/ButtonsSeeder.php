<?php

use Illuminate\Database\Seeder;

class ButtonsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('buttons')->insert([
            'rightclick' => 0,
            'inspect' => 0,
            'goto' => 1,
            'color' => 0,
        ]);
    }
}
