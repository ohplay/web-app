<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(HeaderTranslationsSeeder::class);

        $this->call(ConfigsSeeder::class);
        
        $this->call(UsersSeeder::class);
        
        $this->call(ButtonsSeeder::class);

        $this->call(SeosSeeder::class);
        
        $this->call(FooterTranslationsSeeder::class);
        
        $this->call(PackagesSeeder::class);
        
        $this->call(PopoverTranslationsSeeder::class);
        
        $this->call(MenusSeeder::class);
        
        $this->call(HomeTranslationsSeeder::class);



    }
}
