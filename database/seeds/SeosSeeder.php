<?php

use Illuminate\Database\Seeder;

class SeosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('seos')->insert([
            'fb' => NULL,
            'google' => NULL,
            'metadata' => '{"en":"this ts a next hour"}',
            'description' => '{"en":"OH Play"}',
            'keyword' => '{"en":"OH Play"}',
        ]);
    }
}
