<?php

use Illuminate\Database\Seeder;

class PopoverTranslationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('popover_translations')->insert([
            'key' => 'mins',
            'value' => '{"en":"Mins","nl":"mins2","de":"Minuten"}'
        ]);
        DB::table('popover_translations')->insert([
            'key' => 'season',
            'value' => '{"en":"Season","nl":"season2","de":"Jahreszeit"}'
        ]);
        DB::table('popover_translations')->insert([
            'key' => 'all age',
            'value' => '{"en":"All age","nl":"all age2","de":"jedes Alter"}'
        ]);
        DB::table('popover_translations')->insert([
            'key' => 'read more',
            'value' => '{"en":"Read more","nl":"Read more2","de":"Weiterlesen"}'
        ]);
        DB::table('popover_translations')->insert([
            'key' => 'less',
            'value' => '{"en":"Less","nl":"Less2","de":"Weniger"}'
        ]);
        DB::table('popover_translations')->insert([
            'key' => 'play',
            'value' => '{"en":"Play Now","nl":"play2","de":"abspielen"}'
        ]);
        DB::table('popover_translations')->insert([
            'key' => 'watch trailer',
            'value' => '{"en":"Watch trailer","nl":"watch trailer2","de":"Trailer ansehen"}'
        ]);
        DB::table('popover_translations')->insert([
            'key' => 'add to watchlist',
            'value' => '{"en":"Add to watchlist","nl":"add to watchlist2","de":"Auf die Beobachtungsliste"}'
        ]);
        DB::table('popover_translations')->insert([
            'key' => 'remove from watchlist',
            'value' => '{"en":"Remove  from watchlist","nl":"remove  from watchlist2","de":"aus der Beobachtungsliste entfernen"}'
        ]);
        DB::table('popover_translations')->insert([
            'key' => 'subtitles',
            'value' => '{"en":"Subtitles","nl":"subtitles2","de":"Untertitel"}'
        ]);
    }
}
