<?php

use Illuminate\Database\Seeder;

class FooterTranslationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('footer_translations')->insert([
            'key' => 'terms and condition',
            'value' => '{"en":"Terms and condition","nl":"Terms and condition2","de":"Gesch\u00e4ftsbedingung"}'
        ]);
        DB::table('footer_translations')->insert([
            'key' => 'privacy policy',
            'value' => '{"en":"Privacy policy","nl":"Privacy policy2","de":"Datenschutz-Bestimmungen"}'
        ]);
        DB::table('footer_translations')->insert([
            'key' => 'refund policy',
            'value' => '{"en":"Privacy policy","nl":"Privacy policy2","de":"Datenschutz-Bestimmungen"}'
        ]);
        DB::table('footer_translations')->insert([
            'key' => 'help',
            'value' => '{"en":"Help","nl":"Help2","de":"Hilfe"}'
        ]);
        DB::table('footer_translations')->insert([
            'key' => 'corporate',
            'value' => '{"en":"Corporate","nl":"Corporate2","de":"Unternehmen"}'
        ]);
        DB::table('footer_translations')->insert([
            'key' => 'sitemap',
            'value' => '{"en":"Sitemap","nl":"Sitemap2","de":"Seitenverzeichnis"}'
        ]);
        DB::table('footer_translations')->insert([
            'key' => 'subscribe',
            'value' => '{"en":"Subscribe","nl":"Subscribe2","de":"Abonnieren"}'
        ]);
        DB::table('footer_translations')->insert([
            'key' => 'subscribe text',
            'value' => '{"en":"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.","nl":"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.2","de":"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod."}'
        ]);
    }
}
