<?php

use Illuminate\Database\Seeder;

class HomeTranslationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('home_translations')->insert([
            'key' => 'watch next tv series and movies',
            'value' => '{"en":"Watch Next TV Series And Movies","nl":"Watch Nexot TV Seriesee And Moviessk","de":"Schaue n\u00e4chste TV-Serie und Filme"}',
        ]);
        DB::table('home_translations')->insert([
            'key' => 'watch next movies',
            'value' => '{"en":"Watch Next Movies","nl":"Watch Next Movies5","de":"Sieh dir die n\u00e4chsten Filme an"}',
        ]);
        DB::table('home_translations')->insert([
            'key' => 'watch next tv series',
            'value' => '{"en":"Watch Next TV Series","nl":"Watch Next TV Series5","de":"Sehen Sie sich die n\u00e4chste TV-Serie an"}',
        ]);
        DB::table('home_translations')->insert([
            'key' => 'view all',
            'value' => '{"en":"View all","nl":"View all5","de":"Alle ansehen"}',
        ]);
        DB::table('home_translations')->insert([
            'key' => 'featured',
            'value' => '{"en":"Featured","nl":"featured5","de":"gekennzeichnet"}',
        ]);
        DB::table('home_translations')->insert([
            'key' => 'movies in',
            'value' => '{"en":"Movies  in","nl":"movies  in5","de":"Filme in"}',
        ]);
        DB::table('home_translations')->insert([
            'key' => 'tv shows in',
            'value' => '{"en":"Tv Shows in","nl":"tv shows in5","de":"Fernsehshows in"}',
        ]);
        DB::table('home_translations')->insert([
            'key' => 'at the big screen at home',
            'value' => '{"en":"at the big screen at home","nl":"at the big screen at home5","de":"auf dem gro\u00dfen Bildschirm zu Hause"}',
        ]);
        DB::table('home_translations')->insert([
            'key' => 'recently added',
            'value' => '{"en":"Recently Added","nl":"Recently Added5","de":"K\u00fcrzlich hinzugef\u00fcgt"}',
        ]);
        DB::table('home_translations')->insert([
            'key' => 'found for',
            'value' => '{"en":"Found for","nl":"found for5","de":"gefunden f\u00fcr"}',
        ]);
        DB::table('home_translations')->insert([
            'key' => 'directors',
            'value' => '{"en":"Directors","nl":"Directors5","de":"Direktoren"}',
        ]);
        DB::table('home_translations')->insert([
            'key' => 'starring',
            'value' => '{"en":"Starring","nl":"Starring5","de":"Mit"}',
        ]);
        DB::table('home_translations')->insert([
            'key' => 'genres',
            'value' => '{"en":"Genres","nl":"Genres5","de":"Genres"}',
        ]);
        DB::table('home_translations')->insert([
            'key' => 'audio languages',
            'value' => '{"en":"Audio Languages","nl":"Audio Languages5","de":"Audio-Sprachen"}',
        ]);
        DB::table('home_translations')->insert([
            'key' => 'customers also watched',
            'value' => '{"en":"Customers also watched","nl":"Customers also watched5","de":"Kunden haben auch zugeschaut"}',
        ]);
        DB::table('home_translations')->insert([
            'key' => 'episodes',
            'value' => '{"en":"Episodes","nl":"Episodes5","de":"Episoden"}',
        ]);
        DB::table('home_translations')->insert([
            'key' => 'series',
            'value' => '{"en":"Series","nl":"Series5","de":"Serie"}',
        ]);
        DB::table('home_translations')->insert([
            'key' => 'frequently asked questions',
            'value' => '{"en":"Frequently Asked Questions","nl":"Frequently Asked Questions5","de":"H\u00e4ufig gestellte Fragen"}',
        ]);
        DB::table('home_translations')->insert([
            'key' => 'movies',
            'value' => '{"en":"Movies","nl":"Movies5","de":"Filme"}',
        ]);
        DB::table('home_translations')->insert([
            'key' => 'tv shows',
            'value' => '{"en":"Tv Shows","nl":"Tv Shows5","de":"Fernsehshows"}',
        ]);
        DB::table('home_translations')->insert([
            'key' => '',
            'value' => '',
        ]);
    }
}
