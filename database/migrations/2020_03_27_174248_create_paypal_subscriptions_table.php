<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaypalSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paypal_subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('payment_id')->nullable(TRUE);
            $table->string('user_name')->nullable(TRUE);
            $table->integer('package_id')->nullable(TRUE);
            $table->double('price', 8, 2)->nullable(FALSE);
            $table->tinyInteger('status')->nullable(FALSE);
            $table->string('method')->nullable(FALSE);
            $table->timestamp('subscription_from')->nullable(TRUE);
            $table->timestamp('subscription_to')->nullable(TRUE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paypal_subscriptions');
    }
}
