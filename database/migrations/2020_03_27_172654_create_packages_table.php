<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('plan_id');
            $table->string('name');
            $table->string('currency');
            $table->double('amount', 8, 2);
            $table->string('interval');
            $table->integer('interval_count')->nullable(TRUE);
            $table->string('trial_period_days')->nullable(TRUE);
            $table->tinyInteger('screen')->nullable(TRUE);
            $table->tinyInteger('status')->nullable(TRUE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
