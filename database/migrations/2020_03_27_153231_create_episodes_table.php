<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEpisodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('episodes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('seasons_id'); // Esta columna necesita ser multiple key
            $table->string('tmdb_id')->nullable(TRUE);
            $table->integer('episode_no')->nullable(TRUE);
            $table->string('title');
            $table->char('tmdb')->nullable(TRUE);
            $table->string('duration')->nullable(TRUE);
            $table->text('detail')->nullable(TRUE);
            $table->string('a_language')->nullable(TRUE);
            $table->tinyInteger('subtitle')->nullable(TRUE); // Debería ser de tamaño 1
            $table->string('subtitle_list')->nullable(TRUE);
            $table->string('subtitle_files')->nullable(TRUE);
            $table->string('released')->nullable(TRUE);
            $table->char('type')->nullable(FALSE)->default('E');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('episodes');
    }
}
