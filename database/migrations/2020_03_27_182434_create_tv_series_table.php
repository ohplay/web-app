<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTvSeriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tv_series', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('tmdb_id')->nullable(TRUE);
            $table->char('tmdb')->nullable(TRUE);
            $table->string('thumbnail')->nullable(TRUE);
            $table->string('poster')->nullable(TRUE);
            $table->string('genre_id')->nullable(TRUE);
            $table->text('detail')->nullable(TRUE);
            $table->double('rating', 8, 2)->nullable(TRUE);
            $table->double('episode_runtime', 8, 2)->nullable(TRUE);
            $table->string('maturity_rating', 8, 2)->nullable(TRUE);
            $table->tinyInteger('featured')->nullable(FALSE)->default(0);
            $table->char('type')->nullable(FALSE)->default('T');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tv_series');
    }
}
