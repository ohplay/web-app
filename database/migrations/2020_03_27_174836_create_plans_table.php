<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('image')->nullable(TRUE);
            $table->string('email')->unique('email');
            $table->string('password');
            $table->string('plan')->nullable(TRUE);
            $table->date('dob')->nullable(TRUE);
            $table->string('mobile')->nullable(TRUE);
            $table->string('stripe_id')->nullable(TRUE);
            $table->string('card_brand')->nullable(TRUE);
            $table->string('card_last_four')->nullable(TRUE);
            $table->timestamp('trial_ends_at')->nullable(TRUE);
            $table->tinyInteger('is_admin')->nullable(FALSE)->default(0);
            $table->string('remember_token', 100)->nullable(TRUE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
