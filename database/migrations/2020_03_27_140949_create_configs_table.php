<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('logo')->nullable(TRUE);
            $table->string('favicon')->nullable(TRUE);
            $table->string('title')->nullable(TRUE);
            $table->string('w_name')->nullable(TRUE);
            $table->string('w_email')->nullable(TRUE);
            $table->string('stripe_pub_key')->nullable(TRUE);
            $table->string('stripe_secret_key')->nullable(TRUE);
            $table->string('paypal_mar_email')->nullable(TRUE);
            $table->string('currency_code')->nullable(TRUE);
            $table->string('currency_symbol')->nullable(FALSE);
            $table->string('invoice_add')->nullable(TRUE);
            $table->tinyInteger('prime_main_slider')->default(1);
            $table->tinyInteger('prime_genre_slider')->default(1);
            $table->tinyInteger('prime_footer')->default(1);
            $table->tinyInteger('prime_movie_single')->default(1);
            $table->text('terms_condition')->nullable(TRUE);
            $table->text('privacy_pol')->nullable(TRUE);
            $table->text('refund_pol')->nullable(TRUE);
            $table->text('copyright')->nullable(TRUE);
            $table->tinyInteger('stripe_payment')->default(1);
            $table->tinyInteger('paypal_payment')->default(1);
            $table->tinyInteger('payu_payment')->default(1);
            $table->tinyInteger('preloader')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configs');
    }
}
