<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandingPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landing_pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable(TRUE);
            $table->string('heading')->nullable(TRUE);
            $table->text('detail')->nullable(TRUE);
            $table->tinyInteger('button')->default(1);
            $table->string('button_text')->nullable(TRUE);
            $table->string('button_link')->nullable(TRUE);
            $table->tinyInteger('left')->default(1);
            $table->integer('position')->nullable(TRUE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('landing_pages');
    }
}
