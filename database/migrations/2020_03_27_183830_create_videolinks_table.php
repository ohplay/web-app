<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideolinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videolinks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('movie_id')->nullable(TRUE);
            $table->unsignedInteger('episode_id')->nullable(TRUE);
            $table->string('source_file')->nullable(TRUE);
            $table->string('assets_thumbnails')->nullable(TRUE);
            $table->string('ready_url')->nullable(TRUE);
            $table->string('url_360')->nullable(TRUE);
            $table->string('url_480')->nullable(TRUE);
            $table->string('url_720')->nullable(TRUE);
            $table->string('url_1080')->nullable(TRUE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videolinks');
    }
}
