<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tmdb_id')->nullable(TRUE);
            $table->string('title');
            $table->string('duration')->nullable(TRUE);
            $table->string('thumbnail')->nullable(TRUE);
            $table->string('poster')->nullable(TRUE);
            $table->char('tmdb')->nullable(TRUE);
            $table->string('director_id')->nullable(TRUE);
            $table->string('actor_id')->nullable(TRUE);
            $table->string('genre_id')->nullable(TRUE);
            $table->string('trailer_url')->nullable(TRUE);
            $table->text('detail')->nullable(TRUE);
            $table->integer('rating')->nullable(TRUE);
            $table->string('maturity_rating')->nullable(TRUE);
            $table->tinyInteger('subtitle')->nullable(TRUE);
            $table->string('subtitle_list')->nullable(TRUE);
            $table->string('subtitle_files')->nullable(TRUE);
            $table->integer('publish_year')->nullable(TRUE);
            $table->string('released')->nullable(TRUE);
            $table->tinyInteger('featured')->nullable(TRUE);
            $table->tinyInteger('series')->nullable(TRUE);
            $table->string('a_language')->nullable(TRUE);
            $table->string('audio_files')->nullable(TRUE);
            $table->char('type')->default('M');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
