<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('coupon_code');
            $table->double('percent_off', 8, 2)->nullable(TRUE);
            $table->string('currency');
            $table->double('amount_off', 8, 2)->nullable(TRUE);
            $table->string('duration');
            $table->string('max_redemptions')->nullable(TRUE);
            $table->timestamp('redeem_by')->nullable(TRUE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon_codes');
    }
}
