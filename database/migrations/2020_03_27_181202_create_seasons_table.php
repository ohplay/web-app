<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seasons', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tv_series_id');
            $table->string('tmdb_id')->nullable(TRUE);
            $table->bigInteger('season_no')->nullable(FALSE);
            $table->char('tmdb')->nullable(TRUE);
            $table->string('publish_year')->nullable(TRUE);
            $table->string('thumbnail')->nullable(TRUE);
            $table->string('poster')->nullable(TRUE);
            $table->string('actor_id')->nullable(TRUE);
            $table->string('a_language')->nullable(TRUE);
            $table->tinyInteger('subtitle')->nullable(TRUE);
            $table->string('subtitle_list')->nullable(TRUE);
            $table->text('detail')->nullable(TRUE);
            $table->tinyInteger('featured')->nullable(FALSE)->default(0);
            $table->char('type')->nullable(FALSE)->default('S');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seasons');
    }
}
