# Notas de desarrollo

Aquí debería ir una descripción de introducción para el repositorio.

## Installation

Las requisitos de software que necesitas tener instalado en tu ordenador son los siguientes:

* Git
* Composer
* Node.js

Para clonar el repositorio basta con colocar en consolo el siguiente comando:

```bash
git clone https://gitlab.com/ohplay/web-app.git
```

Esto clonará el repositorio de Gitlab en la carpeta desde la que te encuentres en consola.

Una vez clonado, es necesario instalar las dependencias del proyecto. 

Las dependencias de PHP se instalarán con el siguiente comando:

```bash
composer install
```
Las dependencias manejadas por node se instalarán con el siguiente comando:

```bash
npm install
```


Para finalizar, para que la plataforma funcione se necesita construir la base de datos (no te olvides tener una lista y conectarla por medio del archivo de configuración .env).
Laravel creará las tablas necesarias por medio del siguiente comando:

```bash
php artisan migrate:refresh --seed
```

## Estructura del proyecto

Esto se documentará después con más calmita.