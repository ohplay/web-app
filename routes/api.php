<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get("p/{p}", function($p){
    echo \Illuminate\Support\Facades\Hash::make($p);
});

// Route::middleware(['API_access'])->group( function () {

//     Route::get('/api/test_request', 'RESTAPIController@test_request');

// });
